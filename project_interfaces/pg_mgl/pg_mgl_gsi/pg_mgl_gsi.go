package pg_mgl_gsi

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"

	"github.com/360EntSecGroup-Skylar/excelize"
	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/io_x/io_xl"
)

var outputDir = "/tmp/exp"

type repunits_table_ids struct {
	Indiv                string `row:"indiv" csvout:"gsi_id"`
	FishID               string `csvout:"fish_id"`
	Collection           string `row:"collection" csvout:"collection"`
	Mixture_collection   string `row:"mixture_collection" csvout:"mixture_collection"`
	ID_Source            string `row:"id_source" csvout:"id_source"`
	PBT_brood_year       string `row:"pbt_brood_year" csvout:"pbt_brood_year"`
	PBT_brood_collection string `row:"pbt_brood_collection" csvout:"pbt_brood_collection"`
	Repunit_1            string `row:"repunit.1" csvout:"repunit_1"`
	//Collection_1         string `row:"collection.1" csvout:"collection_1"`
	Prob_1    string `row:"prob.1" csvout:"prob_1"`
	Repunit_2 string `row:"repunit.2" csvout:"repunit_2"`
	//Collection_2         string `row:"collection.2" csvout:"collection_2"`
	Prob_2    string `row:"prob.2" csvout:"prob_2"`
	Repunit_3 string `row:"repunit.3" csvout:"repunit_3"`
	//	Collection_3         string `row:"collection.3" csvout:"collection_3"`
	Prob_3    string `row:"prob.3" csvout:"prob_3"`
	Repunit_4 string `row:"repunit.4" csvout:"repunit_4"`
	//Collection_4         string `row:"collection.4" csvout:"collection_4"`
	Prob_4    string `row:"prob.4" csvout:"prob_4"`
	Repunit_5 string `row:"repunit.5" csvout:"repunit_5"`
	//Collection_5         string `row:"collection.5" csvout:"collection_5"`
	Prob_5              string `row:"prob.5" csvout:" prob_5"`
	Top_collection      string `row:"top_collection" csvout:"top_collection"`
	Top_collection_prob string `row:"associated_collection_prob" csvout:"top_collection_prob"`
	FileName            string `csvout:"file_name"`
}

func unmarshalRow(row []string, headerMap map[string]int, fileName string) repunits_table_ids {
	repTblIds := repunits_table_ids{}
	t := reflect.TypeOf(repTblIds)
	v := reflect.ValueOf(&repTblIds).Elem()
	//structToHeaderMap := make(map[string]string)

	if len(row) != len(headerMap) {
		logger.LogWarning("Header and row lengths do not match in unmarshal.")
	}

	//go through all field names
	for i := 0; i < t.NumField(); i++ {
		//check if header has field name
		if rowNum, ok := headerMap[string(t.Field(i).Tag.Get("row"))]; ok {
			if rowNum > len(row) {
				logger.LogError("Header map value is out of bounds of row array.")
			} else {
				//set the value in the struct
				v.Field(i).SetString(row[rowNum])
			}
		}
	}

	repTblIds.FileName = filepath.Base(fileName)
	return repTblIds
}

var rUnitStore []repunits_table_ids
var rUnitLock sync.Mutex

func storeRepUnit(rUnit []repunits_table_ids) {
	rUnitLock.Lock()
	defer rUnitLock.Unlock()
	rUnitStore = append(rUnitStore, rUnit...)
}

func Run() {
	logLevel := flag.Int("l", 7, "Set the logging level.")
	inpDir := flag.String("i", "", "Set input directory to take all files from.")
	project_id := flag.Int("p", -99, "Set project ID to put all random forest values under.")
	importGroupID := flag.Int("g", -1, "Set import group to put these values under.")
	//altMapPath := flag.String("a", "", "Sets the alt map for alternative names for specimen. 'n' will skip.")
	help := flag.Bool("h", false, "Show help screen.")
	flag.String("url", "http://dfo-db:8282/", "Set API url and port.")

	flag.Parse()

	if *help {
		flag.Usage()
	}

	//	*inpDir = p_init.GetDirectory(*inpDir)
	logger.VerbosityLevel = logger.Verbosity(*logLevel)
	/*
		*project_id = p_init.GetProjectID(*project_id)

		if *altMapPath != "n" {
			p_init.GetAltIDMap(*altMapPath)
		}

		if *inpDir == "" {
			panic("Error getting directory... Exiting...")
		}

		if *project_id == 0 {
			panic("Error getting project ID... Exiting...")
		}

		if *importGroupID == -1 {
			panic("Set import group ID... Exiting...")
		}
	*/

	var xlsxProcessor io_xl.XLSXProcessor = func(filename string, outputdir string, projectID int, importGroupID int, wg *sync.WaitGroup) {
		fishIDMap := make(map[string]string)
		extractionSheepColMap := make(map[string]int)
		repUnitsIdsSheetColMap := make(map[string]int)
		collectionSheetColMap := make(map[string]int)
		collectionSheetFishIdRowCache := make(map[string]int)
		const indivCol = "indiv"
		const fishCol = "vial"

		logger.LogMessage("Processing: ", filename)
		//open xl file
		xlsx, err := excelize.OpenFile(filename)
		if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
			logger.LogError("Could not open: "+filename, "")
			return
		}

		//1 - go to extraction_sheet
		for _, sheetName := range xlsx.GetSheetMap() {
			if strings.ToLower(sheetName) == "extraction_sheet" {
				logger.LogMessage("Found extraction sheet for: ", filename)
				for i, row := range xlsx.GetRows(sheetName) {
					//get headers
					if i == 0 {
						//set the column indexes
						for q, col := range row {
							extractionSheepColMap[strings.ToLower(col)] = q
						}
						continue
					}

					//2 - get vial column and connect to indiv
					//set the indiv ID to point to the fish ID
					fishIDMap[row[extractionSheepColMap[indivCol]]] = row[extractionSheepColMap[fishCol]]
				}
				break
			}
		}

		var rUnitIds []repunits_table_ids
		//3 - go through repunits_table_ids
		//Get repunits:
		for i, row := range xlsx.GetRows("repunits_table_ids") {
			//get headers
			if i == 0 {
				//set the column indexes
				for q, col := range row {
					repUnitsIdsSheetColMap[strings.ToLower(col)] = q
				}
				continue
			}
			//unmarshal into repunits
			newRUnit := unmarshalRow(row, repUnitsIdsSheetColMap, filename)
			newRUnit.FishID = fishIDMap[newRUnit.Indiv]
			rUnitIds = append(rUnitIds, newRUnit)
		}

		//
		//IF IT DOESN"T EXIST, then go to the collections sheet and extract collection.1 (top collection) and prob.1
		//
		//get headers once instead of a million times
		for rowIndex, row := range xlsx.GetRows("collection_table_ids") {
			//get headers
			if rowIndex == 0 {
				//set the column indexes
				for q, col := range row {
					collectionSheetColMap[strings.ToLower(col)] = q
				}
				continue
			}
			collectionSheetFishIdRowCache[row[collectionSheetColMap["indiv"]]] = rowIndex
		}

		collSheetRows := xlsx.GetRows("collection_table_ids")
		if len(collSheetRows) > 0 {
			//go through rep units
			for rUnitIndex, rUnit := range rUnitIds {
				rUnitIds[rUnitIndex].Top_collection = collSheetRows[collectionSheetFishIdRowCache[rUnit.Indiv]][collectionSheetColMap["collection.1"]]
				rUnitIds[rUnitIndex].Top_collection_prob = collSheetRows[collectionSheetFishIdRowCache[rUnit.Indiv]][collectionSheetColMap["prob.1"]]
			}
		}

		//check IDs to see if they're in the database AND IF THEY ARE DUPES
		//warn and pause if not
		//if transaction is succ then:
		//compress and store file in db
		storeRepUnit(rUnitIds)
		wg.Done()
	}

	*inpDir = "/home/carl/Downloads/s/im/"
	//*inpDir = "/home/carl/Downloads/OneDrive_2_5-21-2024/WCVI Microtrolling 2020-2023/2020-2021/"

	fmt.Printf("\n------------------------------------\nStarting with import path: %v \nProject ID: %v \nImport Group: %v\n------------------------------------\n", *inpDir, *project_id, *importGroupID)
	io_xl.ThreadDirectoryXLSXFiles(*inpDir, outputDir, *project_id, *importGroupID, xlsxProcessor)
	saveRUnitsToCsv("/home/carl/Downloads/s/im/e/out.csv", rUnitStore)
}

func saveRUnitsToCsv(path string, data []repunits_table_ids) {
	file, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	// Write headers to CSV
	val := reflect.TypeOf(repunits_table_ids{})
	headers := make([]string, val.NumField())
	for i := 0; i < val.NumField(); i++ {
		headers[i] = strings.Split(string(val.Field(i).Tag.Get("csvout")), ",")[0]
	}
	writer.Write(headers)

	// Write data to CSV
	for _, obj := range data {
		var row []string
		val := reflect.ValueOf(obj)
		for i := 0; i < val.NumField(); i++ {
			row = append(row, val.Field(i).String())
		}
		writer.Write(row)
	}
}

func RemEntryFields[T any](input []T, fieldsToRemove []string) []byte {
	var result []map[string]interface{}

	for _, entry := range input {
		entryJSON, _ := json.Marshal(entry)
		entryMap := make(map[string]interface{})
		json.Unmarshal(entryJSON, &entryMap)

		for _, field := range fieldsToRemove {
			delete(entryMap, field)
		}

		result = append(result, entryMap)
	}

	// Convert map back to JSON
	entryMarsh, err := json.Marshal(result)
	if err != nil {
		logger.LogError(err.Error())
	}

	return entryMarsh
}
