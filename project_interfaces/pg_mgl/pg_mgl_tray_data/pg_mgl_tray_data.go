package pg_mgl_tray_data

import (
	"encoding/csv"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_specimen"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_tray"
)

func Run() {
	inputFileName := flags.CreateFlag[string]("-i", "", "Tray directory location.")
	dbPushUrl := flags.CreateFlag[string]("-pushurl", "http://dfo-db:8283/", "Database URL to use for push.")
	flags.CreateFlag[string]("-geturl", "http://dfo-db:8282/", "Database URL to use for get.")
	altIdMapPath := flags.CreateFlag[string]("-alt", "", "Path to xlsx with alt map. The headings should be 'correct_id' and 'alt_id'.")

	flags.CheckHelpFlag()

	if inputFileName != "" {
		flags.DeleteFlag("-i")
	}

	if altIdMapPath != "" {
		logger.CheckForErrorWithLevel(
			object_processing_specimen.LoadAltMapIdWithPath(altIdMapPath),
			nil,
			thread_safe_error_handling.PANIC_ERR,
		)
	}

	// Get a list of all CSV files in the input directory
	files, err := filepath.Glob(filepath.Join(inputFileName, "*.csv"))
	if err != nil {
		panic(err)
	}
	if len(files) == 0 {
		fmt.Println("No CSV files found in the specified directory.")
		return
	}

	// Process each CSV file
	for _, file := range files {
		logger.LogMessage("Processing: ", file)

		skipFile := false

		f, err := os.Open(file)
		if err != nil {
			panic(err)
		}

		reader := csv.NewReader(f)
		reader.FieldsPerRecord = -1 // To handle inconsistent number of fields

		records, err := reader.ReadAll()
		if err != nil {
			f.Close()
			panic(err)
		}
		f.Close()

		invalidTrayRecords, trayRecords := object_processing_tray.ProcessTray(records)

		if invalidEntryN := len(invalidTrayRecords); invalidEntryN > 0 {
			submit := ""

		outer:
			for {
				fmt.Println("There were ", invalidEntryN, " invalid records for ", file)
				fmt.Println("Should this tray be skipped 's', the invalid entries be excluded 'e', added as controls 'c', or save to a csv 'v'?")
				fmt.Scanln(&submit)

				switch submit {
				case "s":
					logger.LogMessage("Skipping submission for: ", file)
					skipFile = true
					break outer
				case "e":
					logger.LogMessage("Excluding invalid entries.")
					break outer
				case "c":
					logger.LogMessage("Adding invalid entries as controls.")
					for _, entry := range invalidTrayRecords {
						ic := true
						entry.IsControl = &ic
						trayRecords = append(trayRecords, entry)
					}
					break outer
				default:
					logger.LogError("Unknown selection.")
				}
			}
		}

		if !skipFile {
			if !logger.CheckForErrorWithLevel(
				json_net_x.SubmitMarshal(dbPushUrl+"mgl/mgl-trays", json_net_x.INSERT, trayRecords),
				nil, thread_safe_error_handling.PANIC_ERR,
			) {
				logger.LogMessage("Done with file: ", file)
			}
		}
	}

}
