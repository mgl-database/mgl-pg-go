package pg_mgl_normalized_data

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
)

type Row struct {
	SpecimenID string
	Assay      string
	Value      string
	PlateNum   string
	PlateName  string
}

var specimenColumns = []string{"impwiz", "specimen_id", "biomarktbl", "#BiomarkTBL"}
var plateNames = []string{"plate_name"}

var platNumbers = []string{"plate_num", "chip"}

func checkHeaders(headers map[string]int) (err error, specimenCol string, plateNumCol string, plateNameCol string) {
	specFound := false
	plateNumerFound := false

	for _, specimenColumn := range specimenColumns {
		if _, ok := headers[strings.ToLower(specimenColumn)]; ok {
			logger.LogMessage("Found specimen column: ", specimenColumn)
			specFound = true
			specimenCol = strings.ToLower(specimenColumn)
		}
	}

	for _, plateColumn := range platNumbers {
		if _, ok := headers[strings.ToLower(plateColumn)]; ok {
			logger.LogMessage("Found plate number column: ", plateColumn)
			plateNumerFound = true
			plateNameCol = strings.ToLower(plateColumn)
		}
	}

	for _, plateName := range plateNames {
		if _, ok := headers[strings.ToLower(plateName)]; ok {
			logger.LogMessage("Found plate name column: ", plateName)
			plateNameCol = strings.ToLower(plateName)
		}
	}

	if !specFound {
		log.Print("Specimen ID/biomarktbl/impwiz column is missing!")
		return errors.New("Specimen ID/biomarktbl/impwiz column is missing!"), "", "", ""
	}

	if !plateNumerFound {
		log.Print("Plate number column is missing!")
		return errors.New("Plate number column is missing!"), "", "", ""
	}

	return nil, specimenCol, plateNameCol, plateNameCol
}

func Run() {
	specimenColumn := ""
	platNumber := ""
	plateName := ""

	inputFileName := flags.CreateFlag[string]("-i", "", "Normalized file location.")

	flags.CheckHelpFlag()

	if inputFileName != "" {
		flags.DeleteFlag("-i")
	}

	if inputFileName == "" {
		for {
			fmt.Println("Enter input file, enter 'exit' to exit:")
			fmt.Scanln(&inputFileName)
			if inputFileName == "exit" {
				return
			} else if inputFileName != "" {
				break
			}
		}
	}

	file, err := os.Open(inputFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := csv.NewReader(file)

	rows, err := reader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	//to store header locations
	headers := make(map[string]int)

	//get headers
	for i, col := range rows[0] {
		headers[strings.ToLower(col)] = i
	}

	//check them
	err, specimenColumn, platNumber, plateName = checkHeaders(headers)
	if err != nil {
		logger.LogError(err.Error())
	}

	var result []Row

	for i, row := range rows {
		if i == 0 {
			continue // Skip the header row
		}

		specimenID := row[headers[specimenColumn]]

		for key, val := range headers {
			if key == strings.ToLower(specimenColumn) || key == strings.ToLower(platNumber) || key == strings.ToLower(plateName) {
				continue
			}
			result = append(result, Row{
				SpecimenID: specimenID,
				Assay:      key,
				Value:      row[val],
				PlateNum:   row[headers[platNumber]],
				PlateName:  row[headers[plateName]],
			})
		}
	}

	// Write the result to a new CSV file
	outputFile, err := os.Create("norm_processed.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer outputFile.Close()

	writer := csv.NewWriter(outputFile)
	err = writer.Write([]string{specimenColumn, "assay_name", "normalized_foldchange", "tray_num", "tray_name"})
	if err != nil {
		log.Fatal(err)
	}

	for _, row := range result {
		err := writer.Write([]string{row.SpecimenID, row.Assay, row.Value, row.PlateNum, row.PlateName})
		if err != nil {
			log.Fatal(err)
		}
	}

	writer.Flush()

	logger.LogMessage("Done.")
}
