package json_types_random_forest

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
)

func init() {
	getClassifiers()
}

type classifiers []struct {
	ClassifierName string `json:"classifier_name"`
}

var Classifier_resp classifiers

func getClassifiers() {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	response, err := http.Get(dfoUrl + "mgl/mgl-metadata_biomarker_classifiers")
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	jsonErr := json.Unmarshal(responseData, &Classifier_resp)

	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return
	}

}
