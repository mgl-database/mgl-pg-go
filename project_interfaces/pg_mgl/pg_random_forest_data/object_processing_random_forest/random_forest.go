package object_processing_random_forest

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	json_mgl_random_forest "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl/random_forest"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_edna_specimen_ids"
	object_processing_specimen "gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_specimen"

	//"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_random_forest_data/map_interactions"
	"strings"
)

type random_forest_all struct {
}

func mapColToClass(row []string, colClassMap map[string]int, classifier string) error {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}
	input := ""
	if len(row) == 0 {
		err := errors.New("Col to class row is empty")
		logger.LogError(err.Error(), "")
		return err
	}
	for _, colVal := range row {
		colVal = strings.ReplaceAll(colVal, " ", "")
		colVal = strings.ToLower(colVal)
		if colVal == "" || strings.ReplaceAll(strings.ToLower(colVal), " ", "") == "sampleid" || strings.ReplaceAll(strings.ToLower(colVal), " ", "") == "tray" {
			continue
		}
		classifierToUse := strconv.Itoa(GetClassifier(classifier)[0].ClassifierID)
		responseData, err := json_net_x.HttpResponseApiRequest(
			dfoUrl+"mgl/mgl-metadata_biomarker_classifiers_classes?class_name="+colVal+"&classifier_id="+classifierToUse,
			json_net_x.GET, nil)

		if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
			return err
		}

		if strings.Contains(string(responseData), "error") {
			logger.LogError(string(responseData), "")
			return errors.New(string(responseData))
		}

		//unmarshal
		var classes json_mgl_random_forest.Classes
		jsonErr := json.Unmarshal(responseData, &classes)
		if jsonErr != nil {
			logger.LogError("Cannot unmarshal classes: "+jsonErr.Error(), "")
			return jsonErr
		}
		if len(classes) == 0 {
			//try input new class
			fmt.Printf("Could not find class in database. Create new class with this value: %s for this classifier: %s ?\n", colVal, classifier)
			fmt.Scanln(&input)
			if input == "y" {
				createNewClass(classifier, colVal)
				if err := mapColToClass(row, colClassMap, classifier); err == nil {
					return nil
				}
			}
			//if no input or an error above, panic
			err := errors.New("Col to class CLASSES arr is empty for classifier: " + classifier + ", class: " + colVal + ", Ret classifier: " + classifierToUse)
			logger.LogError(err.Error(), "")
			panic("Col to class CLASSES arr is empty for: " + classifier + ", class: " + colVal + ", Ret classifier: " + classifierToUse)
		}
		//need to add some error checks
		colClassMap[colVal] = classes[0].ClassID
	}

	return nil
}

func GetClassifier(classifierToFind string) json_mgl_random_forest.Classifiers {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	classifierToFind = object_processing_specimen.CleanIds(classifierToFind)

	if classifierToFind == "" {
		logger.LogError("Classifier name cannot be empty.", "")
	}

	//get classifier API call
	responseData, err := json_net_x.HttpResponseApiRequest(
		dfoUrl+"mgl/mgl-metadata_biomarker_classifiers?classifier_name="+classifierToFind,
		json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return nil
	}

	//unmarshal
	var classifiers json_mgl_random_forest.Classifiers
	err = json.Unmarshal(responseData, &classifiers)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return nil
	}

	if len(classifiers) == 0 || len(classifiers) > 1 {
		logger.LogError("----------------------------------Classifier not found or not unique: "+classifierToFind, "")
		return nil
	}

	if classifiers[0].ClassifierName == "" {
		logger.LogError("---------------------------------Classifier not found: "+classifierToFind, "")
		return nil
	}

	return classifiers
}

func mapColNamesToRowPos(cols []string, colMap map[string]int) {
	for i, colVal := range cols {
		if colVal == "" || strings.ReplaceAll(strings.ToLower(colVal), " ", "") == "sampleid" || strings.ReplaceAll(strings.ToLower(colVal), " ", "") == "tray" {
			continue
		}
		colMap[strings.ReplaceAll(colVal, " ", "")] = i
	}
}

// TODO: Need to put all of the to be inserted value sinto a JSON that is passed to the API
// then the API can pass these to PSQL as a single transaction that can revert anything on failure
func CreateRandForestValues(rows [][]string, classifierName string, projectID int, importGroupID int) {
	if len(rows) == 0 {
		return
	}

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	// find classifier
	classifier := GetClassifier(classifierName)

	//map the column names to the column numbers in the xlsx rows
	colMap := make(map[string]int)
	mapColNamesToRowPos(rows[0], colMap)

	//map column names to database class IDs
	colToClassID := make(map[string]int)
	mapColToClass(rows[0], colToClassID, classifier[0].ClassifierName)

	trayColumn := -1

	for i, col := range rows[0] {
		if strings.ReplaceAll(strings.ToLower(col), " ", "") == "tray" {
			trayColumn = i
		}
	}

	if trayColumn == -1 {
		logger.LogMessage(classifierName, " has no tray column.")

		//panic(classifierName + " has no tray column.")
	}

	idToUse := -1
	columnToUse := ""
	endPointToUse := ""

	for i, row := range rows {
		apiReturn := ""

		if i == 0 { //skip header row
			continue
		}

		spID, ednaID, correctName, err := object_processing_edna_specimen_ids.CheckEdnaAndSpecimenIdMatch(row[0])

		if err != nil {
			logger.LogWarning(err.Error())
			continue
		}

		if spID != -1 {
			idToUse = spID
			columnToUse = "specimen_id"
			endPointToUse = "_specimen"
		} else if ednaID != -1 {
			idToUse = ednaID
			columnToUse = "edna_id"
			endPointToUse = "_edna"
		}

		if idToUse == -1 {
			panic("ID not set for: " + classifierName)
		}

		//go through each class
		for className, _ := range colMap {
			className = strings.ReplaceAll(className, " ", "")
			//get value for this class
			value, err := strconv.ParseFloat(row[colMap[className]], 64)
			if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
				return
			}

			//get tray
			trayID, err := strconv.Atoi(row[trayColumn])
			if err != nil {
				panic("Tray not numeric: " + row[trayColumn] + " For: " + className)
			}
			//should really check the API here to see if the tray exists..... However, this should be enforced in the database
			if trayID <= 0 {
				panic("Invalid tray: " + strconv.Itoa(trayID) + " For: " + className)
			}

			//map class to class ID
			//this works by using the class name to find the column header, then using the column header to find the class ID from the first row[0] (the header row)
			//need to remember to set the replace all and to lower here as we are referencing the original data in the map
			classID := colToClassID[strings.ToLower(strings.ReplaceAll(rows[0][colMap[className]], " ", ""))]
			if classID == 0 {
				println(colMap[className])
				panic("Class ID is nil. Class name: " + className)
			}
			isCtl := false

			//if the name in this row is the same as the correct name, then we don't need an alt
			if correctName == row[0] {
				apiReturn, err = json_net_x.BlindApiRequest(
					fmt.Sprintf(dfoUrl+"mgl/create_new_rand_forest_value%v?class_id=%v&%v=%v&value=%v&control_study_fish=%v&project_id=%v&random_forest_specimen_name=%v&import_group_id=%v&tray_id=%v",
						endPointToUse, classID, columnToUse, idToUse, value, isCtl, projectID, "", importGroupID, trayID), json_net_x.PATCH, nil)
			} else { //otherwise we need to store the alt name
				apiReturn, err = json_net_x.BlindApiRequest(
					fmt.Sprintf(dfoUrl+"mgl/create_new_rand_forest_value%v?class_id=%v&%v=%v&value=%v&control_study_fish=%v&project_id=%v&random_forest_specimen_name=%v&import_group_id=%v&tray_id=%v",
						endPointToUse, classID, columnToUse, idToUse, value, isCtl, projectID, row[0], importGroupID, trayID), json_net_x.PATCH, nil)
			}

			if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.PANIC_ERR) {
				return
			}

			if strings.Contains(string(apiReturn), "error") {
				logger.LogError(string(apiReturn), "")
			}

		}
	}
}

func CreateNewClassifier(classifierName string, classes []string) error {

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	classifierName = object_processing_specimen.CleanIds(classifierName)
	if classifierName == "" {
		logger.LogError("Classifier name cannot be empty.", "")
	}
	fmt.Printf("Creating classifier: %s\n", classifierName)

	//create new classifier
	if _, err := json_net_x.BlindApiRequest(fmt.Sprintf(dfoUrl+"mgl/create_new_classifier?classifier_name=%s&classifier_creator_id=%d", classifierName, getDbUser()),
		json_net_x.PATCH, nil); err == nil {
		for _, class := range classes {
			createNewClass(classifierName, object_processing_specimen.CleanIds(class))
		}
	} else {
		println("Classifier: '" + classifierName + "' not made. Error: " + err.Error())
		return err
	}
	return nil
}

func createNewClass(classifierName string, class string) error {
	var classifier json_mgl_random_forest.Classifiers
	classifierName = object_processing_specimen.CleanIds(classifierName)
	class = object_processing_specimen.CleanIds(class)

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	//get classifier assigned ID
	classifierBytes, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-metadata_biomarker_classifiers?classifier_name="+classifierName,
		json_net_x.GET, nil)
	if err != nil {
		return err
	}
	jsonErr := json.Unmarshal(classifierBytes, &classifier)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return jsonErr
	}
	//if classifier found, add classes
	if classifier != nil && len(classifier) > 0 {
		if _, err := json_net_x.BlindApiRequest(dfoUrl+"mgl/create_new_class?class_name="+class+"&classifier_id="+strconv.Itoa(classifier[0].ClassifierID),
			json_net_x.PATCH, nil); err != nil {
			return err
		}
	} else {
		return errors.New("Classifier not found. Classes will not be made.")
	}
	return nil
}

// show all users and return a selected user
func getDbUser() int {
	usrID := ""
	fmt.Println("------------------------------------")
	fmt.Println("What is the ID of this classifier creator? Type 'list' to see all users.")
	fmt.Scanln(&usrID)

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	var users json_mgl.Users
	finalval := 0

	if usrID == "list" {
		//print all user IDs
		usr_raw, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/shared-users", json_net_x.GET, nil)
		if err != nil {
			fmt.Println(err)
			finalval = getDbUser()
		}
		json.Unmarshal(usr_raw, &users)

		for _, user := range users {
			fmt.Println(fmt.Sprintf("ID: %v | %v | %v | %v", user.User_id, user.First_name, user.Last_name, user.Email))
		}
		finalval = getDbUser()
	} else if val, err := strconv.Atoi(usrID); err == nil {
		if val > 0 {
			return val
		} else {
			fmt.Println("User ID must be more than 0.")
			finalval = getDbUser()
		}
	} else {
		fmt.Println(err)
		finalval = getDbUser()
	}
	return finalval
}
