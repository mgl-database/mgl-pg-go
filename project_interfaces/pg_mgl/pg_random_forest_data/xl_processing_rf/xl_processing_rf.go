package xl_processing_rf

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"

	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_specimen"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_random_forest_data/object_processing_random_forest"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// for multi threading
var wg sync.WaitGroup

// just a function to loop through xlsx files and create new threads
func ProcessXlsxFiles(inputdir string, outputdir string, projectID int, importGroupID int) {
	files, err := os.ReadDir(inputdir)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if strings.HasSuffix(file.Name(), ".xlsx") && !strings.HasPrefix(file.Name(), "~lock.") {
			//put on new thread
			wg.Add(1)
			go processXlsx(filepath.Join(inputdir, file.Name()), outputdir, projectID, importGroupID) //here
		}
	}
	wg.Wait()
}

func makeOutputDir(outputDir string) error {
	//make output folder
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		err = os.Mkdir(outputDir, 0755)
		if err != nil {
			logger.LogWarning("Could not make directory: "+outputDir, "")
			return err
		}
	}
	return nil
}

func writeCSV(outputFileAndDir string, selectedClassifier string, rows [][]string) {
	splitPath := strings.Split(outputFileAndDir, "/")
	splitPath = splitPath[0 : len(splitPath)-1]

	var parentDir string
	for i, elem := range splitPath {
		if i == len(splitPath) {
			break
		}
		parentDir += elem + "/"
	}

	fmt.Println(parentDir)

	//make output dir
	if dirErr := makeOutputDir(parentDir); dirErr != nil {
		logger.LogError("Could not make directory during file: "+outputFileAndDir, "")
		return
	}

	csvFile, err := os.Create(outputFileAndDir)
	if err != nil {
		log.Fatal(err)
	}
	defer csvFile.Close()

	csvWriter := csv.NewWriter(csvFile)

	//write csv
	for i, row := range rows {
		if i == 0 {
			row = append(row, "classifier")
		} else {
			row = append(row, selectedClassifier)
		}
		err := csvWriter.Write(row)
		if err != nil {
			log.Fatal(err)
		}
	}
	csvWriter.Flush()
}

func findClasses(headerRow []string) []string {
	var foundClasses []string
	var classesCorrect string
	//find the classes in the sheet
	for _, column := range headerRow {
		column = object_processing_specimen.CleanIds(column)
		if column == "" || strings.ReplaceAll(strings.ToLower(column), " ", "") == "sampleid" || strings.ReplaceAll(strings.ToLower(column), " ", "") == "tray" {
			continue
		}
		foundClasses = append(foundClasses, column)
	}

	if len(foundClasses) < 1 {
		return nil
	}

	//confirm these are the correct classes
	fmt.Println("Are these classes correct:")
	for _, class := range foundClasses {
		fmt.Println(class)
	}

	fmt.Scanln(&classesCorrect)
	if classesCorrect == "y" {
		return foundClasses
	} else {
		return nil
	}
}

func findClassifier(classifierName string, headerRow []string) bool {
	var createClassifier string
	var classes []string
	classifierName = object_processing_specimen.CleanIds(classifierName)

	if classifierName == "" {
		return false
	}

	//check api to see if it exists
	if object_processing_random_forest.GetClassifier(classifierName) != nil {
		return true //if it does exist, return true
	}

	//if not, confirm with user to create a new one with this name
	fmt.Println("-------------------------------------------------Create new classifier: ", classifierName+"?")
	fmt.Scanln(&createClassifier)
	if createClassifier == "y" {
		classes = findClasses(headerRow)
		if classes == nil {
			fmt.Println("No classes defined. Reverting this classifier creation.")
			return false
		}
		err := object_processing_random_forest.CreateNewClassifier(classifierName, classes)
		if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
			return false
		}
	} else {
		return false
	}
	return true
}

func processSheet(xlsx *excelize.File, projectID int, sheetName string, importGroupID int) {
	defer wg.Done()
	fmt.Println("Processing : " + sheetName)

	//check if this classifier exists, or is created, if not, don't process this sheet
	if !findClassifier(sheetName, xlsx.GetRows(sheetName)[0]) {
		fmt.Println("Could not find classifier for sheet: " + sheetName)
		return
	}

	//process the rows for this sheet
	object_processing_random_forest.CreateRandForestValues(xlsx.GetRows(sheetName), sheetName, projectID, importGroupID)
	fmt.Println(sheetName + " done.")
}

// process the sheets in the xlsx
func processSheets(xlsx *excelize.File, projectID int, importGroupID int) {
	for _, sheetName := range xlsx.GetSheetMap() {
		wg.Add(1)
		processSheet(xlsx, projectID, sheetName, importGroupID) //here
	}
}

// process individual xlsx file
func processXlsx(filePath string, outputDir string, projectID int, importGroupID int) {
	defer wg.Done()

	//open file
	xlsx, err := excelize.OpenFile(filePath)
	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		logger.LogError("Could not open: "+filePath, "")
		return
	}

	//get the sheets from the xlsx
	processSheets(xlsx, projectID, importGroupID)
}
