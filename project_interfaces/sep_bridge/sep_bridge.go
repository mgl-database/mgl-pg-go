package sep_bridge

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	Logging "gitlab.com/UrsusArcTech/logger"

	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/get"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/db"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/epi"
	dailyhatchery "gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/epi/daily_hatchery"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/push"
)

func containsOnlyLetters(s string) bool {
	// Regular expression to match only letters (no numbers)
	reg := regexp.MustCompile("^[a-zA-Z]+$")
	return reg.MatchString(s)
}
func Run() {
	epiBaseUrl := "https://five.epicollect.net/api/export/entries/sep-datasheet-2024?map_index=&form_ref=ef143c0558a4444c8a65ef9968dab54b_"
	urlDbEndpointBase := "http://127.0.0.1:8283/mgl/mgl-sep-"
	urlAqDbEndpoint := urlDbEndpointBase + "aq"
	urlHatcheryDbEndpoint := urlDbEndpointBase + "hatchery"
	urlCohortDbEndpoint := urlDbEndpointBase + "cohort"
	urlCohortClearDbEndpoint := urlDbEndpointBase + "clear_cohort_links"
	urlCohortLinkDbEndpoint := urlDbEndpointBase + "cohort_link"
	urlCurrentCohortsDbEndpoint := urlDbEndpointBase + "current_cohort_link"
	urlFishingsetDbEndpoint := urlDbEndpointBase + "fishingset"
	urlEDNASampleTypeDbEndpoint := urlDbEndpointBase + "edna_sampling_type"
	urlFishSampleTypeDbEndpoint := urlDbEndpointBase + "fish_sampling_type"
	urlEDNAMetadataDbEndpoint := urlDbEndpointBase + "edna_metadata"
	urlFishMetadataDbEndpoint := urlDbEndpointBase + "fish_metadata"

	//form types
	var aqPages []epi.AQ_form_1
	var hatcheryPages []epi.Hatchery_form_2
	var cohortPages []epi.Cohort_form_3
	var cohortLink []db.CohortLink
	var currentCohorts []db.CurrentCohorts
	var fishingSetPages []epi.Fishing_set_form_4
	var samplingTypePages []epi.Sampling_type_form_5
	var ednaBranchPages []epi.EDNA_branch_1
	var tissueSampleBranch []epi.Tissue_sample_branch_2

	//mgl types
	var dbMetadataSpecimen []db.MetadataSpecimen
	var dbMetadataEDNA []db.MetadataEdna

	get.SetToken(4875, "BS5HSGpj6or5eoSIhqtGy9JJkhnTfPExRslBcApI")

	//get
	get.GetEntryPages(&aqPages, get.GetToken(), epiBaseUrl+"656f55f506612")
	get.GetEntryPages(&hatcheryPages, get.GetToken(), epiBaseUrl+"656fb226c129e")
	get.GetEntryPages(&cohortPages, get.GetToken(), epiBaseUrl+"656fb24ac12a2")
	//generate cohort links
	db.GenerateCohortLink(cohortPages, &cohortLink)
	db.FindCurrentCohorts(cohortPages, cohortLink, &currentCohorts)

	//make all cohorts to upper
	for _, pages := range cohortPages {
		for i := range pages.Data.Entries {
			cohortRntry := &pages.Data.Entries[i]
			cohortRntry.CohortNameForDb = strings.ToUpper(cohortRntry.CohortNameForDb)
		}
	}
	for i := range cohortLink {
		cohortLink[i].CurrID = strings.ToUpper(cohortLink[i].CurrID)
		cohortLink[i].PrevID = strings.ToUpper(cohortLink[i].PrevID)
	}

	//check cohort names for dupes
	{
		var nameTemp []string
		for _, pages := range cohortPages {
			for _, entry := range pages.Data.Entries {
				nameTemp = append(nameTemp, entry.CohortNameForDb)
			}
		}
		if checkForDupes(nameTemp) {
			Logging.LogError("Found duplicates in cohorts.")
			panic("Duplicates found, exiting...")
		}
	}

	//continue get
	get.GetEntryPages(&fishingSetPages, get.GetToken(), epiBaseUrl+"656fb259c12a4")
	get.GetEntryPages(&samplingTypePages, get.GetToken(), epiBaseUrl+"656fb7fcc12b8")
	//edna for epi
	get.GetEntryPages(&ednaBranchPages, get.GetToken(), epiBaseUrl+"656fb7fcc12b8&branch_ref=ef143c0558a4444c8a65ef9968dab54b_656fb7fcc12b8_656fb80dc12b9")
	//tissue for epi and mmgl
	get.GetEntryPages(&tissueSampleBranch, get.GetToken(), epiBaseUrl+"656fb7fcc12b8&branch_ref=ef143c0558a4444c8a65ef9968dab54b_656fb7fcc12b8_656fb815c12ba")
	epi.ConvertStructFromEpi(db.MetadataSpecimenConversionMap, &dbMetadataSpecimen, &tissueSampleBranch, true)

	//push
	push.SubmitDatabaseAQ(aqPages, push.UPSERT)
	push.SubmitEpiEntryToUrl(aqPages, urlAqDbEndpoint, push.UPSERT)
	push.SubmitEpiEntryToUrl(hatcheryPages, urlHatcheryDbEndpoint, push.UPSERT)
	push.SubmitEpiEntryToUrl(cohortPages, urlCohortDbEndpoint, push.UPSERT)
	push.SubmitEpiEntryToUrl(fishingSetPages, urlFishingsetDbEndpoint, push.UPSERT)

	//split edna and gill form
	epi.SetSubmissionType(epi.EDNA)
	push.SubmitEpiEntryToUrl(samplingTypePages, urlEDNASampleTypeDbEndpoint, push.UPSERT)
	epi.SetSubmissionType(epi.GILL)
	push.SubmitEpiEntryToUrl(samplingTypePages, urlFishSampleTypeDbEndpoint, push.UPSERT)

	//check tissue for dupes
	{
		var nameTemp []string
		for _, branch := range tissueSampleBranch {
			for _, entry := range branch.Data.Entries {
				nameTemp = append(nameTemp, entry.MGLPrefix+entry.TissueMGLNum)
			}
		}
		if checkForDupes(nameTemp) {
			Logging.LogError("Found duplicates in tissue.")
			panic("Duplicates found, exiting...")
		}
	}
	push.SubmitEpiEntryToUrl(tissueSampleBranch, urlFishMetadataDbEndpoint, push.UPSERT)

	//submit mgl tables
	//tissue/specimen
	for i := range dbMetadataSpecimen {
		//build info for each record
		dbMetadataSpecimen[i].GetDatabaseInfo()
	}
	push.SubmitMarshal("http://127.0.0.1:8283/mgl/mgl-specimen_metadata-uid_pk", push.UPSERT, dbMetadataSpecimen)

	//this has to be submitted first because it creates the entries in the db that the split below depend on
	push.SubmitEpiEntryToUrl(ednaBranchPages, urlEDNAMetadataDbEndpoint, push.UPSERT)

	//edna - different order to tissue cos it's easier to convert after splitting up the repeat
	for i := range ednaBranchPages {
		//apply prefix to originals and split
		ednaBranchPages[i].SplitEDNAIDsByFilterAndApplyPrefix()
	}
	epi.ConvertStructFromEpi(db.MetadataEDNAConversionMap, &dbMetadataEDNA, &ednaBranchPages, false)

	//check eDNA for dupes
	{
		var nameTemp []string
		for _, eDNA := range dbMetadataEDNA {
			nameTemp = append(nameTemp, *eDNA.SampleID)
		}
		if checkForDupes(nameTemp) {
			Logging.LogError("Found duplicates in eDNA.")
			panic("Duplicates found, exiting...")
		}
	}

	//need to get the AQs
	for i := range dbMetadataEDNA {
		dbMetadataEDNA[i].GetDatabaseInfo()
	}

	//insert the edna data or MGL table
	push.SubmitMarshal("http://127.0.0.1:8283/mgl/mgl-edna_metadata_uuid_pk", push.UPSERT, dbMetadataEDNA)
	//now that the eDNA is in, we need to fix the replicates... So, we need to retrieve the ID of the replicates and originals and then insert the original ID
	//into the replicate replicate_of column
	//could change this to the map and stick it all in a single json?
	//intIDMap := make(map[int]int)
	for replicate, original := range epi.EDNAReplicates {
		replID, err := db.GetEDNAID(replicate)
		if err != nil {
			Logging.LogError("Cannot find replicate: " + replicate + " " + err.Error())
			continue
		}
		orig, err := db.GetEDNAID(original)
		if err != nil {
			Logging.LogError("Cannot find original: " + original + " " + err.Error())
			continue
		}

		//USING NON-TRANSACTION API
		push.SubmitParamsURL("http://127.0.0.1:8282/mgl/insert_mgl_edna_metadata_replica_of?replicate_of="+fmt.Sprint(orig)+"&original_id="+fmt.Sprint(replID), "PATCH")
		//	intIDMap[replID] = orig
	}

	//just deleting all of the links to make life easy and recreating them
	push.RunScript(urlCohortClearDbEndpoint)

	push.SubmitMarshal(urlCohortLinkDbEndpoint, push.INSERT, cohortLink)
	push.SubmitMarshal(urlCurrentCohortsDbEndpoint, push.INSERT, currentCohorts)

	submitDailyData()
}

func a() {
	get.SetToken(4875, "BS5HSGpj6or5eoSIhqtGy9JJkhnTfPExRslBcApI")

	epiBaseUrl := "https://five.epicollect.net/api/export/entries/sep-datasheet-2024?map_index=&form_ref=ef143c0558a4444c8a65ef9968dab54b_"
	urlDbEndpointBase := "http://127.0.0.1:8283/mgl/mgl-sep-"

	//urlCohortClearDbEndpoint := urlDbEndpointBase + "clear_cohort_links"
	urlCohortLinkDbEndpoint := urlDbEndpointBase + "cohort_link"
	urlCurrentCohortsDbEndpoint := urlDbEndpointBase + "current_cohort_link"

	var cohortPages []epi.Cohort_form_3
	var cohortLink []db.CohortLink
	var currentCohorts []db.CurrentCohorts

	get.GetEntryPages(&cohortPages, get.GetToken(), epiBaseUrl+"656fb24ac12a2")
	//generate cohort links
	db.GenerateCohortLink(cohortPages, &cohortLink)
	db.FindCurrentCohorts(cohortPages, cohortLink, &currentCohorts)

	//push.RunScript(urlCohortClearDbEndpoint)
	push.SubmitMarshal(urlCohortLinkDbEndpoint, push.UPSERT, cohortLink)
	push.SubmitMarshal(urlCurrentCohortsDbEndpoint, push.UPSERT, currentCohorts)
}

func submitDailyData() {
	epiBaseUrl := "https://five.epicollect.net/api/export/entries/sep-hatchery-daily?map_index=&form_ref=ab60043bb03246e3b22d85ff7b98016a_"
	urlDbEndpointBase := "http://127.0.0.1:8283/mgl/mgl-sep-daily-hatchery"
	urlAqDbEndpoint := urlDbEndpointBase + "-aq"
	urlHatcheryDbEndpoint := urlDbEndpointBase
	urlDailyDbEndpoint := urlDbEndpointBase + "-data"

	var aqPages []dailyhatchery.AQ_form_1
	var hatcheryPages []dailyhatchery.Hatchery_form_2
	var dailyFormData []dailyhatchery.Daily_data_form_3

	get.SetToken(4940, "hmvOBrAgXOp5Cjc2VqLYkZuDHBMWfBaGQtktZv4H")

	get.GetEntryPages(&aqPages, get.GetToken(), epiBaseUrl+"65cd29f11fcf5")
	get.GetEntryPages(&hatcheryPages, get.GetToken(), epiBaseUrl+"65cd2b5fdc873")
	get.GetEntryPages(&dailyFormData, get.GetToken(), epiBaseUrl+"65cd2be9dc87b")

	//TO DO: should link these (and the other sep) to the AQ and hatchery tables in MGL...
	push.SubmitEpiEntryToUrl(aqPages, urlAqDbEndpoint, push.UPSERT)
	push.SubmitEpiEntryToUrl(hatcheryPages, urlHatcheryDbEndpoint, push.UPSERT)
	push.SubmitEpiEntryToUrl(dailyFormData, urlDailyDbEndpoint, push.UPSERT)
}

func checkForDupes(items []string) (hasDupes bool) {
	itemMap := make(map[string]int)
	for _, item := range items {
		itemMap[item]++
	}

	for k, v := range itemMap {
		if v > 1 {
			Logging.LogMessage("Duplicate item: " + k + " count: " + strconv.Itoa(v))
			hasDupes = true
		}
	}
	return hasDupes
}
