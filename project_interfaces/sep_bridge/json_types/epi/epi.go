package epi

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	Logging "gitlab.com/UrsusArcTech/logger"
)

type Epi interface {
	GetLinks() Links
	GetData() interface{}
	GetEntries() []byte
	GetGenericEntries() []interface{}
}

type Links struct {
	Self  string `json:"self"`
	First string `json:"first"`
	Prev  string `json:"prev"`
	Next  string `json:"next"`
	Last  string `json:"last"`
}

type Meta struct {
	Total       int       `json:"total"`
	PerPage     int       `json:"per_page"`
	CurrentPage int       `json:"current_page"`
	LastPage    int       `json:"last_page"`
	From        int       `json:"from"`
	To          int       `json:"to"`
	Newest      time.Time `json:"newest"`
	Oldest      time.Time `json:"oldest"`
}

type TimeWTimeZone string

func (ct *TimeWTimeZone) UnmarshalJSON(b []byte) error {
	str := string(b)

	// Remove quotes
	str = str[1 : len(str)-1]

	// Check if the time string includes a timezone offset
	if !strings.Contains(str, "Z") {
		// If it doesn't, add it
		str += ".000Z"
	}

	*ct = TimeWTimeZone(str)
	return nil
}

type FloatOrString float64

func (f *FloatOrString) UnmarshalJSON(b []byte) error {
	var number float64
	if err := json.Unmarshal(b, &number); err == nil {
		*f = FloatOrString(number)
		return nil
	}

	var str string
	if err := json.Unmarshal(b, &str); err != nil {
		return err
	}

	if str == "" {
		return nil
	}

	number, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return err
	}

	return nil
}

type StringToFloat float64

func (s *StringToFloat) UnmarshalJSON(b []byte) error {
	// Try to convert to string
	var str string
	if err := json.Unmarshal(b, &str); err == nil {
		// If it is a string
		// Trim spaces
		str = strings.TrimSpace(str)

		// Check if empty
		if str == "" {
			s = nil
			return nil
		}

		// Attempt float conversion
		number, err := strconv.ParseFloat(str, 64)
		if err != nil {
			s = nil
			return err // Return the error if conversion fails
		}

		*s = StringToFloat(number)
		return nil
	}

	// If it's not a string
	//need to convert to string first so that float conversion doesn't shit itself
	str = strings.TrimSpace(string(b)) // Convert byte slice to string
	if str == "" {
		s = nil
		return nil
	}

	number, err := strconv.ParseFloat(str, 64)
	if err != nil {
		s = nil
		return err // Return the error if conversion fails
	}

	*s = StringToFloat(number)
	return nil
}

type StringToInt int

func (s *StringToInt) UnmarshalJSON(b []byte) error {
	// Try to convert to string
	var str string
	if err := json.Unmarshal(b, &str); err == nil {
		// If it is a string
		// Trim spaces
		str = strings.TrimSpace(str)

		// Check if empty
		if str == "" {
			s = nil
			return nil
		}

		// Attempt integer conversion
		number, err := strconv.Atoi(str)
		if err != nil {
			s = nil
			return err // Return the error if conversion fails
		}

		*s = StringToInt(number)
		return nil
	}

	// If it's not a string
	str = strings.TrimSpace(string(b)) // Convert byte slice to string
	if str == "" {
		s = nil
		return nil
	}

	number, err := strconv.Atoi(str)
	if err != nil {
		s = nil
		return err // Return the error if conversion fails
	}

	*s = StringToInt(number)
	return nil
}

type StringOrInt string

func (s *StringOrInt) UnmarshalJSON(b []byte) error {
	var str string
	if err := json.Unmarshal(b, &str); err == nil {
		*s = StringOrInt(str)
		return nil
	}

	var number int
	if err := json.Unmarshal(b, &number); err != nil {
		return err
	}

	*s = StringOrInt(strconv.Itoa(number))
	return nil
}

type StrArrToStr string

func (s *StrArrToStr) UnmarshalJSON(b []byte) error {
	var strArr []string
	newStr := ""
	if err := json.Unmarshal(b, &strArr); err == nil {
		for _, str := range strArr {
			newStr += str + ","
		}
		if len(newStr) > 0 {
			*s = StrArrToStr(newStr[0 : len(newStr)-1])
		}
		return nil
	}
	return nil
}

type PSQLDate string

func (s *PSQLDate) UnmarshalJSON(b []byte) error {

	if len(b) == 0 {
		return nil
	}

	str := strings.Trim(string(b), "\"")

	// Parse the input date
	t, err := time.Parse("01/02/2006", str)
	if err != nil {
		Logging.LogError("Error parsing date: " + err.Error())
		return err
	}

	// Format the date in YYYY-MM-DD format
	*s = PSQLDate(t.Format("2006-01-02"))

	return nil
}

type StrToBool bool

func (s *StrToBool) UnmarshalJSON(b []byte) error {

	str := strings.Trim(string(b), "\"")

	if len(str) == 0 {
		return nil
	}

	if str == "N" {
		str = "F"
	} else if str == "Y" {
		str = "T"
	}

	boolValue, err := strconv.ParseBool(str)
	if err != nil {
		Logging.LogError(err.Error())
	}

	*s = StrToBool(boolValue)

	return nil
}

func removeEntryFields[T Epi](structToGet T, fieldsToExclude []string) []byte {
	structToGet.GetData()
	// Convert struct to map
	var dataMap map[string]interface{}
	inrec, _ := json.Marshal(structToGet)
	json.Unmarshal(inrec, &dataMap)

	// Remove unwanted fields
	for _, field := range fieldsToExclude {
		delete(dataMap, field)
	}

	// Convert map back to JSON
	entryMarsh, err := json.Marshal(dataMap)
	if err != nil {
		Logging.LogError(err.Error())
	}

	return entryMarsh
}

// generic wrapper for arrays of types
func ConvertStructFromEpi[T any, R Epi](fieldsToConvert map[string][]string, firstStruct *[]T, epiStruct *[]R, suppressConversionErrors bool) {
	for _, epiPage := range *epiStruct {
		for _, epiEntry := range epiPage.GetGenericEntries() {
			var n T
			//need to get the pointer for the struct by dereferencing it first
			convertStruct(fieldsToConvert, &n, &epiEntry, suppressConversionErrors)
			*firstStruct = append(*firstStruct, n)
		}
	}
}

// convert one struct to another using a map
// the inner array in the map is if we want to combine two fields from the secondStruct into one field in the firstStruct
func convertStruct[T any, R any](fieldsToConvert map[string][]string, firstStruct *T, secondStruct *R, suppressConversionErrors bool) {
	firstStructVals := reflect.ValueOf(&*firstStruct).Elem()
	secondStructVals := reflect.ValueOf(&*secondStruct).Elem()

	// If secondStructVals is an interface, extract the underlying value
	if secondStructVals.Kind() == reflect.Interface {
		secondStructVals = secondStructVals.Elem()
	}

	for k, v := range fieldsToConvert {
		if len(v) == 1 {
			fieldToSet := firstStructVals.FieldByName(k)
			fieldToPull := secondStructVals.FieldByName(v[0])

			if fieldToSet.IsValid() && fieldToSet.CanSet() && fieldToPull.IsValid() {
				switch fieldToSet.Kind() {
				case reflect.String:
					fieldToSet.SetString(fmt.Sprint(fieldToPull.Interface()))
				case reflect.Float32:
					fieldToSet.SetFloat(fieldToPull.Float())
				case reflect.Int:
					fieldToSet.SetInt(fieldToPull.Int())
				case reflect.Pointer:
					if !fieldToPull.IsNil() {
						targetPtr := reflect.New(fieldToSet.Type().Elem())
						targetPtr.Elem().Set(fieldToPull.Elem())
						fieldToSet.Set(targetPtr)
					}
				default:
					Logging.LogError("Data type not found when converting: " + k + " to: " + v[0])
				}
			} else {
				//this is done because some struct vals can just be ignored and we just want to convert the whole data set, but 1 or two wont work. So this means the output wont be clogged.
				if !suppressConversionErrors {
					Logging.LogError("Error converting struct field: " + k + " to: " + v[0])
				}
			}
		} else if len(v) > 1 && len(v) > 0 {
			fieldToSet := firstStructVals.FieldByName(k)

			if fieldToSet.IsValid() && fieldToSet.CanSet() {
				switch fieldToSet.Kind() {
				case reflect.String:
					fieldToSet.SetString(combineStringFields(secondStructVals, v))
				default:
					Logging.LogError("Strings are only allowed when using arrays for conversion: " + k + " to: " + v[0])
				}
			} else {
				Logging.LogError("Error converting struct field in array: " + k + " to: " + v[0])
			}
		}
	}
}

func combineStringFields(structConcreteVals reflect.Value, fieldNames []string) string {
	finalVal := ""
	var fieldToPull reflect.Value
	for _, s := range fieldNames {
		fieldToPull = structConcreteVals.FieldByName(s)
		if fieldToPull.IsValid() {
			finalVal += fmt.Sprint(fieldToPull.Interface())
		}
	}
	return finalVal
}
