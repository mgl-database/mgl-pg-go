package epi

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
)

func (a Fishing_set_form_4) GetLinks() Links {
	return a.Links
}

func (h Fishing_set_form_4) GetData() interface{} {
	return h.Data
}

func (h Fishing_set_form_4) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a Fishing_set_form_4) GetEntries() []byte {
	exclude := []string{"title"}
	entries := make([]interface{}, len(a.Data.Entries))
	for i, v := range a.Data.Entries {
		// Convert the struct to a map
		var entryMap map[string]interface{}
		//marshal the entry
		inrec, _ := json.Marshal(v)
		//unmarshal the entry into the entry map
		json.Unmarshal(inrec, &entryMap)

		// Remove unwanted fields
		for _, exc := range exclude {
			delete(entryMap, exc)
		}

		// Add the map to the entries slice
		entries[i] = entryMap
	}

	entryMarsh, err := json.Marshal(entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}

type Fishing_set_form_4 struct {
	Links `json:"links"`
	Meta  `json:"meta"`
	Data  struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID       string        `json:"ec5_uuid"`
			Ec5ParentUUID string        `json:"ec5_parent_uuid"`
			CreatedAt     TimeWTimeZone `json:"created_at"`
			UploadedAt    TimeWTimeZone `json:"uploaded_at"`
			CreatedBy     string        `json:"created_by"`
			Title         string        `json:"title"`
			Tank          string        `json:"tank"`
			Date          string        `json:"date"`
		} `json:"entries"`
	} `json:"data"`
}
