package epi

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
)

func (a Hatchery_form_2) GetLinks() Links {
	return a.Links
}

func (h Hatchery_form_2) GetData() interface{} {
	return h.Data
}

func (a Hatchery_form_2) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}

func (h Hatchery_form_2) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

type Hatchery_form_2 struct {
	Links `json:"links"`
	Meta  `json:"meta"`
	Data  struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID       string        `json:"ec5_uuid"`
			Ec5ParentUUID string        `json:"ec5_parent_uuid"`
			CreatedAt     TimeWTimeZone `json:"created_at"`
			UploadedAt    TimeWTimeZone `json:"uploaded_at"`
			CreatedBy     string        `json:"created_by"`
			//Title         string    `json:"title"`
			HatcheryName StrArrToStr `json:"hatchery_name"`
		} `json:"entries"`
	} `json:"data"`
}
