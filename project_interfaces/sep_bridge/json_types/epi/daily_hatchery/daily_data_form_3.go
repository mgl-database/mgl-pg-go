package dailyhatchery

import (
	"encoding/json"

	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/epi"

	Logging "gitlab.com/UrsusArcTech/logger"
)

func (a Daily_data_form_3) GetLinks() epi.Links {
	return a.Links
}

func (h Daily_data_form_3) GetData() interface{} {
	return h.Data
}

func (a Daily_data_form_3) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}

func (h Daily_data_form_3) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

type Daily_data_form_3 struct {
	epi.Links `json:"links"`
	epi.Meta  `json:"meta"`
	Data      struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID       string            `json:"ec5_uuid"`
			Ec5ParentUUID string            `json:"ec5_parent_uuid"`
			CreatedAt     epi.TimeWTimeZone `json:"created_at"`
			UploadedAt    epi.TimeWTimeZone `json:"uploaded_at"`
			CreatedBy     string            `json:"created_by"`
			//Title          string            `json:"title"`
			DateDailyData  string             `json:"date_daily_data"`
			Tank           string             `json:"tank"`
			Cohort         string             `json:"cohort"`
			MortalityCount *epi.StringToInt   `json:"mortality_count"`
			TotalCount     *epi.StringToInt   `json:"total_count"`
			MovedTo        *epi.StringOrInt   `json:"moved_to"`
			CountTo        *epi.StringToInt   `json:"count_to"`
			MovedFrom      *string            `json:"moved_from"`
			CountFrom      *epi.StringToInt   `json:"count_from"`
			WaterSource    *epi.StrArrToStr   `json:"water_source"`
			WaterTempC     *epi.StringToFloat `json:"water_temp_c"`
			AvgMassG       *epi.StringToFloat `json:"avg_mass_g"`
			TankDensity    *epi.StringToFloat `json:"tank_density"`
			Treatment      *string            `json:"treatment"`
			Comments       *string            `json:"comments"`
		} `json:"entries"`
	} `json:"data"`
}
