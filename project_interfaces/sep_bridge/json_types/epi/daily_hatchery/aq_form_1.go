package dailyhatchery

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/epi"
)

type AQ_form_1 struct {
	epi.Links `json:"links"`
	epi.Meta  `json:"meta"`
	Data      struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID    string            `json:"ec5_uuid"`
			CreatedAt  epi.TimeWTimeZone `json:"created_at"`
			UploadedAt epi.TimeWTimeZone `json:"uploaded_at"`
			CreatedBy  string            `json:"created_by"`
			//Title      string    `json:"title"`
			Aq string `json:"aq"`
		} `json:"entries"`
	} `json:"data"`
}

func (h AQ_form_1) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a AQ_form_1) GetLinks() epi.Links {
	return a.Links
}

func (a AQ_form_1) GetData() interface{} {
	return a.Data
}

func (a AQ_form_1) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}
