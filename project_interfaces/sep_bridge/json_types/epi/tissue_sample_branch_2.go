package epi

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
)

func (a Tissue_sample_branch_2) GetLinks() Links {
	return a.Links
}

func (h Tissue_sample_branch_2) GetData() interface{} {
	return h.Data
}

func (h Tissue_sample_branch_2) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a Tissue_sample_branch_2) GetEntries() []byte {
	exclude := []string{"title", "MGL_Prefix"}
	entries := make([]interface{}, len(a.Data.Entries))
	var entryMarsh []byte
	var newEntryMarsh []byte
	var err error
	for i, v := range a.Data.Entries {
		// Convert the struct to a map
		var entryMap map[string]interface{}

		//sort suffix
		if v.MGLPrefix != "" {
			v.TissueMGLNum = v.MGLPrefix + v.TissueMGLNum
		}

		//marshal the entry
		inrec, _ := json.Marshal(v)
		//unmarshal the entry into the entry map
		json.Unmarshal(inrec, &entryMap)

		// Remove unwanted fields
		for _, exc := range exclude {
			delete(entryMap, exc)
		}

		// Add the map to the entries slice
		entries[i] = entryMap
	}

	newEntryMarsh, err = json.Marshal(entries)
	if err != nil {
		Logging.LogError(err.Error())
	} else {
		entryMarsh = append(entryMarsh, newEntryMarsh...)
	}
	return entryMarsh
}

type Tissue_sample_branch_2 struct {
	Links `json:"links"`
	Meta  `json:"meta"`
	Data  struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			//NEED TO INSERT THE SPECIMEN ID
			//could also use the UUID?
			//SpecimenSpecimenID   int  `json:"specimen_specimen_id"`
			Ec5BranchOwnerUUID string        `json:"ec5_branch_owner_uuid"`
			Ec5BranchUUID      string        `json:"ec5_branch_uuid"`
			CreatedAt          TimeWTimeZone `json:"created_at"`
			UploadedAt         TimeWTimeZone `json:"uploaded_at"`
			CreatedBy          string        `json:"created_by"`
			//Title              string        `json:"title"`
			Time            string      `json:"Time"`
			Mort            string      `json:"Mort"`
			ParrMarks       StrToBool   `json:"Parr_Marks"`
			Sex             string      `json:"Sex"`
			TissueTypeTaken StrArrToStr `json:"tissue_type_taken"`
			MGLPrefix       string      `json:"MGL_Prefix"`
			TissueMGLNum    string      `json:"tissue_MGL_num"`
			LengthMm        StringToInt `json:"Length_mm"`
			WeightMg        float64     `json:"weight_mg"`
			ClinicalSigns   StrArrToStr `json:"clinical_signs"`
			HistoTaken      string      `json:"Histo_Taken"`
			HistoJarNum     StringOrInt `json:"Histo_Jar_num"`
			SwabTaken       string      `json:"swab_Taken"`
			//Photo           string      `json:"photo"`
			Comments string `json:"Comments"`
		} `json:"entries"`
	} `json:"data"`
}

//these need to be inserted into the mgl tables
//Title              string        `json:"title"`
//Mort            string      `json:"Mort"`
//Sex             string      `json:"Sex"`
//MGLPrefix       string      `json:"MGL_Prefix"`
//TissueMGLNum    string      `json:"tissue_MGL_num"`
//LengthMm        int         `json:"Length_mm"`
//WeightMg        float64     `json:"weight_mg"`
//HistoTaken      string      `json:"Histo_Taken"`
//HistoJarNum     string      `json:"Histo_Jar_num"`
//Photo           string      `json:"photo"`
//Ec5BranchUUID      string        `json:"ec5_branch_uuid"`
//Comments string `json:"Comments"`
