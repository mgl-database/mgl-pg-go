package epi

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
)

type AQ_form_1 struct {
	Links `json:"links"`
	Meta  `json:"meta"`
	Data  struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5_uuid    string        `json:"ec5_uuid"`
			Created_at  TimeWTimeZone `json:"created_at"`
			Uploaded_at TimeWTimeZone `json:"uploaded_at"`
			Created_by  string        `json:"created_by"`
			//Title      string    `json:"title"`
			Aq string `json:"aq"`
		} `json:"entries"`
	} `json:"data"`
}

func (h AQ_form_1) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a AQ_form_1) GetLinks() Links {
	return a.Links
}

func (a AQ_form_1) GetData() interface{} {
	return a.Data
}

func (a AQ_form_1) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}
