package db

import (
	"errors"
	"time"

	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/get"
)

func init() {
	MetadataEDNAConversionMap = make(map[string][]string)
	MetadataEDNAConversionMap["SampleID"] = []string{"Filter1"}
	//	MetadataEDNAConversionMap["SampleID"] = []string{"EdnaMGLPrefix", "Filter1"}
	MetadataEDNAConversionMap["CollectionSamplers"] = []string{"EdnaSampler"}
	MetadataEDNAConversionMap["Date"] = []string{"DateMdy"}
	MetadataEDNAConversionMap["WaterTempC"] = []string{"TempC"}
	MetadataEDNAConversionMap["Weather"] = []string{"Weather"}
	MetadataEDNAConversionMap["DissolvedSolidsMgL"] = []string{"SalinityPpt"}
	MetadataEDNAConversionMap["DoMgL"] = []string{"DOMgL"}
	MetadataEDNAConversionMap["FilterStartTime"] = []string{"TimeStartedFilt"}
	MetadataEDNAConversionMap["FilterType"] = []string{"FilterType"}
	MetadataEDNAConversionMap["FilterComments"] = []string{"Comments"}
	MetadataEDNAConversionMap["FilterEndTime"] = []string{"TimeEndedFilter"}
	MetadataEDNAConversionMap["UniqueID"] = []string{"Ec5BranchUUID"}
	MetadataEDNAConversionMap["SampleVolumeMl"] = []string{"VolumeFilteredL"}
	MetadataEDNAConversionMap["SampleDepthM"] = []string{"DepthFilteredM"}
}

var MetadataEDNAConversionMap map[string][]string

type MetadataEdna struct {
	CollectionSamplers string  `json:"collection_samplers"`   //EdnaSampler     string        `json:"edna_Sampler"`
	Date               string  `json:"date"`                  //DateMdy         PSQLDate      `json:"edna_date"`
	WaterTempC         float32 `json:"water_temp_c"`          //TempC           FloatOrString `json:"Temp_C"`
	Weather            string  `json:"weather"`               //Weather         StrArrToStr   `json:"Weather"`
	DissolvedSolidsMgL float32 `json:"dissolved_solids_mg_l"` //SalinityPpt     FloatOrString `json:"Salinity_ppt"`
	DoMgL              float32 `json:"do_mg_l"`               //DOMgL           FloatOrString `json:"DO_mgL"`
	ReplicateOf        *int    `json:"replicate_of"`          //       `json:"Filter_1"` //Filter2         string        `json:"Filter_2"`	//Filter3         string        `json:"Filter_3"`	//Filter4         string        `json:"Filter_4"`
	FilterStartTime    string  `json:"filter_start_time"`     //TimeStartedFilt string        `json:"Time_started_filt"`
	FilterType         string  `json:"filter_type"`           //FilterType      string        `json:"filter_type"`
	FilterComments     string  `json:"filter_comments"`       //Comments        string        `json:"COMMENTS"`
	FilterEndTime      string  `json:"filter_end_time"`       //TimeEndedFilter string        `json:"Time_ended_filter"`
	UniqueID           string  `json:"unique_id"`             //Ec5BranchUUID      string    `json:"ec5_branch_uuid"`
	//UniqueSet          *string    `json:"unique_set"`
	SampleID       *string `json:"sample_id"` //Filter1         string
	AqEdna         string  `json:"aq_edna"`
	SampleVolumeMl float32 `json:"sample_volume_ml"` //VolumeFilteredL FloatOrString `json:"Volume_Filtered_l"`
	SampleDepthM   float32 `json:"sample_depth_m"`   //DepthFilteredM  FloatOrString `json:"Depth_Filtered_m"`
}

type EDNA struct {
	ID int `json:"id"`
}

type Edna_epi_db struct {
	Aq              string    `json:"aq"`
	CohortNameForDb string    `json:"cohort_name_for_db"`
	Comments        string    `json:"comments"`
	Date            time.Time `json:"date"`
	DepthFilteredM  string    `json:"depth_filtered_m"`
	DoMgl           string    `json:"do_mgl"`
	DoPerc          string    `json:"do_perc"`
	Ec5BranchUUID   string    `json:"ec5_branch_uuid"`
	EdnaSampler     string    `json:"edna_sampler"`
	EdnaMGLPrefix   string    `json:"edna_MGL_Prefix"`
	Filter1         *string   `json:"filter_1"`
	Filter2         *string   `json:"filter_2"`
	Filter3         *string   `json:"filter_3"`
	Filter4         *string   `json:"filter_4"`
	FilterType      string    `json:"filter_type"`
	HatcheryName    string    `json:"hatchery_name"`
	NegativeControl bool      `json:"negative_control"`
	SalinityPpt     string    `json:"salinity_ppt"`
	Tank            string    `json:"tank"`
	TempC           string    `json:"temp_c"`
	Tide            string    `json:"tide"`
	TimeEndedFilter string    `json:"time_ended_filter"`
	TimeStartedFilt string    `json:"time_started_filt"`
	VolumeFilteredL string    `json:"volume_filtered_l"`
	Weather         string    `json:"weather"`
}

func GetEDNAID(name string) (int, error) {
	var edna []EDNA
	get.UnmarshalURL("http://127.0.0.1:8283/mgl/mgl-edna_metadata_uuid_pk?sample_id="+name, &edna, "GET")
	if len(edna) != 0 && edna[0].ID != 0 {
		if len(edna) > 1 {
			return -1, errors.New("Error: more than one eDNA record found with this ID:" + name)
		}
		return edna[0].ID, nil
	}
	return -1, errors.New("Unable to find eDNA name: " + name)
}

func (m *MetadataEdna) GetDatabaseInfo() {
	var sepEDNAView []Edna_epi_db

	//Logging.LogMessage("Getting eDNA info for: " + *m.SampleID)
	//not getting combined S + number IDs....
	get.UnmarshalURL("http://127.0.0.1:8282/mgl/get_sep_edna_by_filter?filter_num="+*m.SampleID, &sepEDNAView, "PATCH")

	if len(sepEDNAView) == 0 {
		Logging.LogError("eDNA API returned empty for: " + *m.SampleID)
		return
	} else {
		m.AqEdna = sepEDNAView[0].Aq
	}
}
