package db

import (
	"errors"

	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/get"
)

type cohort struct {
	ID int `json:"cohort_id"`
}

type tank struct {
	ID int `json:"tank_id"`
}

type waterSource struct {
	ID int `json:"id"`
}

func GetCohortID(cohortName string) (int, error) {
	var cohorts []cohort
	get.UnmarshalURL("http://127.0.0.1:8282/mgl/mgl-sep_cohort_ref_table?cohort_name="+cohortName, &cohorts, "GET")
	if len(cohorts) != 0 && cohorts[0].ID != 0 {
		if len(cohorts) > 1 {
			return -1, errors.New("Error: more than one cohort record found with this ID:" + cohortName)
		}
		return cohorts[0].ID, nil
	}
	return -1, errors.New("Unable to find cohort name: " + cohortName)
}

func GetTankID(tankName string) (int, error) {
	var tanks []tank
	get.UnmarshalURL("http://127.0.0.1:8282/mgl/mgl-sep_tank_ref_table?tank_name="+tankName, &tanks, "GET")
	if len(tanks) != 0 && tanks[0].ID != 0 {
		if len(tanks) > 1 {
			return -1, errors.New("Error: more than one tank record found with this ID:" + tankName)
		}
		return tanks[0].ID, nil
	}
	return -1, errors.New("Unable to find tabk name: " + tankName)
}

func GetWaterSourceID(waterSourceName string) (int, error) {
	var waterSources []waterSource
	get.UnmarshalURL("http://127.0.0.1:8282/mgl/mgl-sep_water_source_ref_table?water_source_name="+waterSourceName, &waterSources, "GET")
	if len(waterSources) != 0 && waterSources[0].ID != 0 {
		if len(waterSources) > 1 {
			return -1, errors.New("Error: more than one water source record found with this ID:" + waterSourceName)
		}
		return waterSources[0].ID, nil
	}
	return -1, errors.New("Unable to find water source name: " + waterSourceName)
}
