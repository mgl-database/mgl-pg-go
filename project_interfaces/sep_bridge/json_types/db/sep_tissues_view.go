package db

import "time"

type SepTissuesView []struct {
	Aq               string    `json:"aq"`
	ClinicalSigns    string    `json:"clinical_signs"`
	CohortNameForDb  string    `json:"cohort_name_for_db"`
	Comments         string    `json:"comments"`
	Date             time.Time `json:"date"`
	Ec5BranchUUID    string    `json:"ec5_branch_uuid"`
	FishSampleDate   time.Time `json:"fish_sample_date"`
	FishTimesSampler string    `json:"fish_times_sampler"`
	HatcheryName     string    `json:"hatchery_name"`
	HistoJarNum      string    `json:"histo_jar_num"`
	HistoTaken       string    `json:"histo_taken"`
	LengthMm         string    `json:"length_mm"`
	Mort             string    `json:"mort"`
	NegativeControl  bool      `json:"negative_control"`
	ParrMarks        bool      `json:"parr_marks"`
	SampleType       string    `json:"sample_type"`
	Sex              string    `json:"sex"`
	Species          string    `json:"species"`
	SwabTaken        bool      `json:"swab_taken"`
	Tank             string    `json:"tank"`
	Time             time.Time `json:"time"`
	TimeDissections  time.Time `json:"time_dissections"`
	TimeFishAnesth   time.Time `json:"time_fish_anesth"`
	TimeFishPulled   time.Time `json:"time_fish_pulled"`
	TissueMglNum     string    `json:"tissue_mgl_num"`
	TissueTypeTaken  string    `json:"tissue_type_taken"`
	WeightMg         string    `json:"weight_mg"`
}
