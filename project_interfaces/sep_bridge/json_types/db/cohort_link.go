package db

import "gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/epi"

type CohortLink struct {
	CurrID string `json:"current_id"`
	PrevID string `json:"previous_id"`
}

type CurrentCohorts struct {
	CohortID string `json:"cohort_id"`
}

func GenerateCohortLink(cohortPages []epi.Cohort_form_3, cohortLink *[]CohortLink) {
	for _, page := range cohortPages {
		for _, entry := range page.Data.Entries {
			for _, prevCohor := range entry.PreviousCohorts {
				if prevCohor == "" {
					continue
				}
				newChLink := CohortLink{
					CurrID: entry.CohortNameForDb,
					PrevID: prevCohor,
				}
				*cohortLink = append(*cohortLink, newChLink)
			}
		}
	}
}

func FindCurrentCohorts(cohortPages []epi.Cohort_form_3, cohortLink []CohortLink, currentCohorts *[]CurrentCohorts) {
	foundCohort := false
	//loop through the cohort link, if it is not in any of the previous list
	//then it is a current cohort
	for _, page := range cohortPages {
		for _, cohortToFind := range page.Data.Entries {
			foundCohort = false
			for _, previousCohorts := range cohortLink {
				if cohortToFind.CohortNameForDb == previousCohorts.PrevID {
					//found, so not current
					foundCohort = true
					break
				}
			}
			//if not found in any previous, add to current list
			if !foundCohort {
				var currentCohort CurrentCohorts
				currentCohort.CohortID = cohortToFind.CohortNameForDb
				*currentCohorts = append(*currentCohorts, currentCohort)
			}
		}
	}
}
