package push

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"os"

	Logging "gitlab.com/UrsusArcTech/logger"

	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/db"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge/json_types/epi"
)

type DBPostType string

const (
	INSERT DBPostType = "INSERT"
	UPSERT            = "UPSERT"
	PATCH             = "PATCH"
)

func SubmitDatabaseAQ(aqForms []epi.AQ_form_1, dbPostType DBPostType) {
	header := make(http.Header)
	aqs := make(map[string]string)
	aqJson := "["

	for _, aqPages := range aqForms {
		for _, aqEntry := range aqPages.Data.Entries {
			aqs[aqEntry.Aq] = aqEntry.Aq
		}
	}

	for _, v := range aqs {
		aqJson += "{\"aq\": \"" + v + "\"},"
	}

	//remove the last comma
	aqJson = aqJson[0 : len(aqJson)-1]
	aqJson += "]"

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	SubmitBytesToUrl("http://127.0.0.1:8283/mgl/mgl-aq", []byte(aqJson), header)
}

func SubmitEpiEntryToUrl[T epi.Epi](newEntry []T, url string, dbPostType DBPostType) {
	header := make(http.Header)
	var allEntries []byte

	Logging.LogMessage("Starting submit for: " + url)

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	for _, entry := range newEntry {
		//this can't be done because you cannot marshall several together into a byte
		//allEntries = append(allEntries, entry.GetEntries()...)
		allEntries = entry.GetEntries()

		//for now just send each page separately as I cba
		SubmitBytesToUrl(url, allEntries, header)
	}

	Logging.LogMessage("Submission done.")
}

func SubmitMarshal[T interface{}](url string, dbPostType DBPostType, all T) {
	header := make(http.Header)

	Logging.LogMessage("Starting for: " + url)

	data, err := json.Marshal(all)
	if err != nil {
		Logging.LogError("Error marshalling data: " + err.Error())
	}

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	SubmitBytesToUrl(url, data, header)
	Logging.LogMessage("Submission done.")

}

func SubmitMarshalOfArrCohortLink[T []db.CohortLink](url string, dbPostType DBPostType, all T) {
	header := make(http.Header)
	var allData []byte

	Logging.LogMessage("Starting for: " + url)

	for _, a := range all {
		data, err := json.Marshal(a)
		if err != nil {
			Logging.LogError("Error marshalling data: " + err.Error())
			return
		}
		allData = append(allData, data...)
	}

	if len(allData) == 0 {
		Logging.LogError("Data empty.")
		return
	}

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	SubmitBytesToUrl(url, allData, header)
	Logging.LogMessage("Submission done.")

}

func SubmitMarshalOfArrCurrCohorts[T []db.CurrentCohorts](url string, dbPostType DBPostType, all T) {
	header := make(http.Header)
	var allData []byte

	Logging.LogMessage("Starting for: " + url)

	for _, a := range all {
		data, err := json.Marshal(a)
		if err != nil {
			Logging.LogError("Error marshalling data: " + err.Error())
			return
		}
		allData = append(allData, data...)
	}

	if len(allData) == 0 {
		Logging.LogError("Data empty.")
		return
	}

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	SubmitBytesToUrl(url, allData, header)
	Logging.LogMessage("Submission done.")

}

func SubmitBytesToUrl(url string, newBytes []byte, header http.Header) {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(newBytes))
	if err != nil {
		Logging.LogError("Error creating post request for: " + url + ". Error: " + err.Error())
		return
	}

	req.Header = header

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		Logging.LogError("Error trying to connect to: " + url + ". Error: " + err.Error())
		return
	}

	if resp == nil {
		Logging.LogError("No response from: " + url + ". No error returned.")
	}

	if resp.StatusCode != http.StatusOK {
		rBody, readErr := io.ReadAll(resp.Body)
		if readErr != nil {
			Logging.LogError("Respose body read error: " + readErr.Error())
		}
		Logging.LogError("Status: " + resp.Status + ". body: " + string(rBody))
		Logging.LogError(resp.Status)
		os.Exit(1)
	}
	defer resp.Body.Close()

}

func SubmitParamsURL(url string, dbPostType DBPostType) {
	header := make(http.Header)

	//Logging.LogMessage("Starting params URL submit for: " + url)

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	req, err := http.NewRequest(string(dbPostType), url, nil)
	if err != nil {
		Logging.LogError("Error creating post request for: " + url + ". Error: " + err.Error())
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		Logging.LogError("Error trying to connect to: " + url + ". Error: " + err.Error())
		return
	}

	if resp.StatusCode != http.StatusOK {
		rBody, readErr := io.ReadAll(resp.Body)
		if readErr != nil {
			Logging.LogError("Respose body read error: " + readErr.Error())
		}
		Logging.LogError("Status: " + resp.Status + ". body: " + string(rBody))
		Logging.LogError(resp.Status)
	}
	defer resp.Body.Close()
}

func RunScript(url string) {
	// Create a new HTTP client
	client := &http.Client{}

	// Create a PATCH request with an empty request body
	req, err := http.NewRequest(PATCH, url, nil)
	if err != nil {
		Logging.LogError("Error creating PATCH request:" + err.Error())
		return
	}

	// Send the request
	resp, err := client.Do(req)
	if err != nil {
		Logging.LogError("Error sending PATCH request:" + err.Error())
		return
	}
	defer resp.Body.Close()

	// Check the response status code
	if resp.StatusCode != http.StatusOK {
		Logging.LogError("PATCH Request failed with status: " + resp.Status)
		return
	}
	Logging.LogMessage("PATCH request done.")
}
