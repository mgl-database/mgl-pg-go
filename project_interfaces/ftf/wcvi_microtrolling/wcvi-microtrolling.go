package wcvi_microtrolling

import (
	"encoding/json"
	"strconv"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x/json_net_epi"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
	epi_ftf_wcvi_microtrolling "gitlab.com/mgl-database/mgl-pg-go/json_types/epi/ftf_wcvi_microtrolling"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x"
)

func Run() {
	databseGetURL := "http://dfo-db:8282"
	databsePushURL := "http://dfo-db:8283"

	epiBaseEntriesUrl := "https://five.epicollect.net/api/export/entries/follow-the-fish-wcvi-microtrolling?map_index=&form_ref=6a199cdd998c4087ae0a5873bd922702_64cbe9760278d"
	epiFishingsetUrl := epiBaseEntriesUrl + "&branch_ref=6a199cdd998c4087ae0a5873bd922702_64cbe9760278d_64ef602ee033b"
	epiOceanographicUrl := epiBaseEntriesUrl + "&branch_ref=6a199cdd998c4087ae0a5873bd922702_64cbe9760278d_65204e06f27c9"
	epiCatchSamplingUrl := epiBaseEntriesUrl + "&branch_ref=6a199cdd998c4087ae0a5873bd922702_64cbe9760278d_6525d6082d305"

	var epiBaseEntries []epi_ftf_wcvi_microtrolling.BaseEntries
	var epiFishingsets []epi_ftf_wcvi_microtrolling.FishingSet
	var epiOceanographic []epi_ftf_wcvi_microtrolling.Oceanographic
	var epiCatchSampling []epi_ftf_wcvi_microtrolling.CatchSampling

	json_net_epi.SetToken(5253, "euTvTEVLzhUMDa6RwA4AFUvTuuLyZ7sdQ1XTyPWG")

	json_net_epi.GetEntryPages(&epiBaseEntries, json_net_epi.GetToken(), epiBaseEntriesUrl)
	json_net_epi.GetEntryPages(&epiFishingsets, json_net_epi.GetToken(), epiFishingsetUrl)
	json_net_epi.GetEntryPages(&epiOceanographic, json_net_epi.GetToken(), epiOceanographicUrl)
	json_net_epi.GetEntryPages(&epiCatchSampling, json_net_epi.GetToken(), epiCatchSamplingUrl)

	//FISGHINSETS -------------------------------------------------------------------
	{
		var DBFishingSets []json_mgl.MetadataFishingset

		fishingsetConversionMap := make(map[string][]string)
		fishingsetConversionMap["BottomDepthFt"] = []string{"Seven3BottomDepthFt"}
		fishingsetConversionMap["StartTime"] = []string{"Seven4SetStartTimeFi"}
		fishingsetConversionMap["SetComments"] = []string{"Seven6SidesFished", "Seven7HookIntervalFt", "Seven8PortCannonballD", "Seven9StarboardCannonb", "Eight0OfPortHooks", "Eight2Comments", "Eight6Comments"}
		fishingsetConversionMap["Timezone"] = []string{"UTMZone"}
		fishingsetConversionMap["Fishingset"] = []string{"Ec5BranchUUID"}
		fishingsetConversionMap["Set"] = []string{"Seven1FishingSet"}
		fishingsetConversionMap["SetComments"] = []string{"Eight6Comments"}
		fishingsetConversionMap["Longitude"] = []string{"Longitude"}
		fishingsetConversionMap["Latitude"] = []string{"Latitude"}
		fishingsetConversionMap["EndTime"] = []string{"Eight3SetEndTimeLast"}
		fishingsetConversionMap["ConvParentUUID"] = []string{"Ec5BranchOwnerUUID"}

		epi.ConvertStructFromEpi(fishingsetConversionMap, &DBFishingSets, &epiFishingsets, false)

		for i := range DBFishingSets {
			//this is just one I created
			DBFishingSets[i].AqID = "ce0988eb-03ca-448b-8b22-e0c06c65c3f1"
			for _, baseEntries := range epiBaseEntries {
				for _, baseEntryEntry := range baseEntries.Data.Entries {

					//get the name from base entry
					if DBFishingSets[i].ConvParentUUID == baseEntryEntry.Ec5UUID {
						DBFishingSets[i].Vessel = baseEntryEntry.SixVesselName
						DBFishingSets[i].SetLocation = baseEntryEntry.FiveFishingLocation
						break
					}
				}

			}
		}

		//remove conversion and database get only columns
		for _, fset := range DBFishingSets {
			dbSubmFishSets := object_processing_x.RemoveEntryFields(fset, []string{"ConvParentUUID", "FishingsetDateAdded", "FishingsetDateUpdated", "FishingsetID"})

			//submit to db
			err := json_net_x.SubmitMarshal(databsePushURL+"/mgl/mgl-metadata_fishingset", json_net_x.DBPostType(json_net_x.UPSERT), dbSubmFishSets)
			if err != nil {
				logger.LogFatal("Error submitting fishingsets: ", err.Error())
			}
		}

		//SPECIMEN -------------------------------------------------------------------
		{
			var DBSpecimen []json_mgl.MetadataSpecimen
			speciesCache := make(map[string]int)
			fsetCache := make(map[string]int)

			specimenConversionMap := make(map[string][]string)
			specimenConversionMap["One05GillVial"] = []string{"FishNum"}
			specimenConversionMap["ForkLengthMm"] = []string{"One01ForkLengthMm"} //need to figure out float vals as db is int....
			specimenConversionMap["SpecimenComments"] = []string{"One17Comments"}
			specimenConversionMap["SpecimenComments2"] = []string{"Nine6ClinicalSignsCh"}
			specimenConversionMap["NonlethalYn"] = []string{"One12LethalSample"}
			specimenConversionMap["convSpeciesName"] = []string{"Nine1Species"}
			specimenConversionMap["convFishingsetID"] = []string{"Eight9FishingSet"}
			specimenConversionMap["ConvParentUUID"] = []string{"Ec5BranchOwnerUUID"}
			specimenConversionMap["SpecimenUniqueID"] = []string{"ec5_branch_uuid"}
			//		specimenConversionMap[""] = []string{""}

			epi.ConvertStructFromEpi(specimenConversionMap, &DBSpecimen, &epiCatchSampling, false)

			//get the fishingsets

			var species []json_mgl.RSpecies
			//need to get species from db - do a loop
			for i, _ := range DBSpecimen {
				//invert nonlethalyn
				if DBSpecimen[i].NonlethalYn == "Y" {
					DBSpecimen[i].NonlethalYn = "N"
				} else if DBSpecimen[i].NonlethalYn == "N" {
					DBSpecimen[i].NonlethalYn = "Y"
				}

				//bit of a hack, but least shitty way to do it - it's matching the DB name
				if DBSpecimen[i].ConvSpeciesName == "Pacific Herring" {
					DBSpecimen[i].ConvSpeciesName = "Herring"
				}

				//get the species
				if speciesCache[DBSpecimen[i].ConvSpeciesName] == 0 {
					err := json_net_x.UnmarshalURL(databseGetURL+"/mgl/mgl-species?common_name="+DBSpecimen[i].ConvSpeciesName, &species, string(json_net_x.GET))
					if err != nil {
						logger.LogError("Error getting species:", DBSpecimen[i].ConvSpeciesName, "for fish: ", DBSpecimen[i].FishNum, ". error: ", err.Error())
					}

					if len(species) == 0 {
						//unknown
						logger.LogWarning("Unknown fish species: ", DBSpecimen[i].ConvSpeciesName, ". Setting species ID to 999...")
						DBSpecimen[i].SpeciesID = 999
						speciesCache[DBSpecimen[i].ConvSpeciesName] = 999
					} else {
						DBSpecimen[i].SpeciesID = species[0].SpeciesID
						speciesCache[DBSpecimen[i].ConvSpeciesName] = species[0].SpeciesID
					}

				} else {
					DBSpecimen[i].SpeciesID = speciesCache[DBSpecimen[i].ConvSpeciesName] //if it's in the cache, just copy
				}

				var fset []json_mgl.MetadataFishingset
				//get the fishingset
				if fsetCache[DBSpecimen[i].ConvParentUUID+strconv.Itoa(DBSpecimen[i].ConvFishingsetID)] == 0 {
					for _, dbFset := range DBFishingSets {
						if dbFset.Set == strconv.Itoa(DBSpecimen[i].ConvFishingsetID) && dbFset.ConvParentUUID == DBSpecimen[i].ConvParentUUID {
							DBSpecimen[i].ConvFishingsetUUID = dbFset.Fishingset
						}
					}

					err := json_net_x.UnmarshalURL(databseGetURL+"/mgl/mgl-metadata_fishingset?fishingset="+DBSpecimen[i].ConvFishingsetUUID, &fset, string(json_net_x.GET))

					if err != nil {
						logger.LogError("Error getting fishingset:", DBSpecimen[i].ConvFishingsetUUID, "for fish: ", DBSpecimen[i].FishNum, ". error: ", err.Error())
					}

					if len(fset) == 0 {
						//unknown
						logger.LogError("Unknown fishingset: ", DBSpecimen[i].ConvFishingsetUUID, " skipping entry....")
						continue
					}

					if len(fset) > 1 {
						logger.LogError("Found more than one fishingset for: ", DBSpecimen[i].ConvFishingsetUUID, " skipping entry....")
						continue
					}

					DBSpecimen[i].SpecimenFishingsetID = fset[0].FishingsetID
					fsetCache[DBSpecimen[i].ConvParentUUID+strconv.Itoa(DBSpecimen[i].ConvFishingsetID)] = fset[0].FishingsetID

				} else {
					DBSpecimen[i].SpecimenFishingsetID = fsetCache[DBSpecimen[i].ConvParentUUID+strconv.Itoa(DBSpecimen[i].ConvFishingsetID)] //if it's in the cache, just copy
				}

			}

			//var toDelete []int

			for i, spec := range DBSpecimen {
				if spec.FishNum == "" {
					DBSpecimen[i].PendingVialAssignment = true
				}
			}
			/*
				for _, i := range toDelete {
					DBSpecimen = object_processing_x.DeleteElement(DBSpecimen, i)
				}*/

			//remove the redundant columns for submission
			for _, spec := range DBSpecimen {
				dbSpecSubm := object_processing_x.RemoveEntryFields(spec, []string{"ConvSpeciesName", "ConvFishingsetID", "ConvFishingsetUUID", "SpecimenSpecimenID"})
				json_net_x.SubmitMarshal(databsePushURL+"/mgl/mgl-specimen_metadata", json_net_x.DBPostType(json_net_x.UPSERT), dbSpecSubm)
			}
		}

	}

}

func RemEntryFields[T any](input []T, fieldsToRemove []string) []byte {
	var result []map[string]interface{}

	for _, entry := range input {
		entryJSON, _ := json.Marshal(entry)
		entryMap := make(map[string]interface{})
		json.Unmarshal(entryJSON, &entryMap)

		for _, field := range fieldsToRemove {
			delete(entryMap, field)
		}

		result = append(result, entryMap)
	}

	// Convert map back to JSON
	entryMarsh, err := json.Marshal(result)
	if err != nil {
		logger.LogError(err.Error())
	}

	return entryMarsh
}
