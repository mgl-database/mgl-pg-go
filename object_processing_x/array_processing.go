package object_processing_x

import (
	"gitlab.com/UrsusArcTech/logger"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
)

func DeleteElement(arr []json_mgl.MetadataSpecimen, index int) []json_mgl.MetadataSpecimen {
	// Check if index is out of range
	if index < 0 || index >= len(arr) {
		logger.LogError("Index out of range")
		return arr
	}

	// Delete the element
	return append(arr[:index], arr[index+1:]...)
}
