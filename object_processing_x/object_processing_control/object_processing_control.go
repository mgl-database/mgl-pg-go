package object_processing_control

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	"gitlab.com/mgl-database/mgl-pg-go/map_x"
)

var controlIdCache = make(map[string]int)

func ControlExists(controlId string) (err error, exists bool) {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	if isAssigned, val := map_x.GetMapAssignmentStrInt(controlIdCache, controlId); isAssigned {
		logger.LogDebugMessage("Found  ID: "+controlId+" in cache, value: "+strconv.Itoa(val), "")
		return nil, true
	}

	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/get_fluidigm_controls_search?control_id=%25"+controlId+"%25", json_net_x.PATCH, nil)
	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return err, false
	}

	var control []json_mgl.ControlFluidigm
	jsonErr := json.Unmarshal(responseData, &control)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return jsonErr, false
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return errors.New(string(responseData)), false
	}

	if len(control) > 0 {
		logger.LogDebugMessage("Found control: "+controlId, "")
		return nil, true
	}

	return nil, false
}
