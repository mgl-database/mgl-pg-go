package object_processing_tray

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"unicode/utf8"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_control"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_edna_specimen_ids"
)

func CheckTrayNumberExists(trayNum int) (bool, error) {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-trays?tray_number="+strconv.Itoa(trayNum), json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return false, err
	}

	var tray []json_mgl.MetadataTray
	jsonErr := json.Unmarshal(responseData, &tray)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return false, jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return false, errors.New(string(responseData))
	}

	if len(tray) < 1 {
		logger.LogDebugMessage("Tray not found: ", trayNum)
		return false, nil
	}

	logger.LogMessage("Found tray: ", trayNum)
	return true, nil

}

func CheckTrayNameExists(trayName string) (bool, error) {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-trays?tray_name="+trayName, json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return false, err
	}

	var tray []json_mgl.MetadataTray
	jsonErr := json.Unmarshal(responseData, &tray)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return false, jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return false, errors.New(string(responseData))
	}

	if len(tray) < 1 {
		logger.LogDebugMessage("Tray not found: ", trayName)
		return false, nil
	}

	logger.LogMessage("Found tray: ", trayName)
	return true, nil

}

func trimWhitespace(field string) string {
	trimmed := strings.TrimSpace(field)
	if len(trimmed) > 0 {
		r, size := utf8.DecodeRuneInString(trimmed)
		if r == '\ufeff' {
			trimmed = trimmed[size:]
		}
	}
	return trimmed
}

func ProcessTray(records [][]string) (invalidTrayRecords []json_mgl.MetadataTray, validTrayRecords []json_mgl.MetadataTray) {
	data := make(map[string]string)
	var trayLayout [][]string

	for _, row := range records {
		if len(row) > 0 {
			key := trimWhitespace(row[0])
			switch key {
			case "Plate name:":
				data["PlateName"] = trimWhitespace(row[1])
			case "Tray #:":
				data["TrayNumber"] = trimWhitespace(row[1])
			case "Species:":
				data["Species"] = trimWhitespace(row[1])
			case "Tissue:":
				data["Tissue"] = trimWhitespace(row[1])
			case "Run by:":
				data["RunBy"] = trimWhitespace(row[1])
			default:
				if len(row) > 1 && trimWhitespace(row[1]) != "" && strings.Join(row, ",") != ",1,2,3,4,5,6,7,8,9,10,11,12" {
					cleanedRow := make([]string, len(row))
					for i, field := range row {
						cleanedRow[i] = trimWhitespace(field)
					}
					trayLayout = append(trayLayout, cleanedRow)
				}
			}
		}

		trayNumber := data["TrayNumber"]
		trayName := data["PlateName"]
		species := data["Species"]
		tissue := data["Tissue"]
		runBy := data["RunBy"]

		var tissues []string
		mixedSingle := "Single"

		if strings.ToLower(tissue) == "mixed" {
			tissues = append(tissues, "other")
			tissue = "unknown_mixed"
			mixedSingle = "Mixed"
		} else if strings.Contains(strings.ToLower(tissue), "and") {
			tissues = strings.Split(tissue, "and")
			tissue = strings.ReplaceAll(strings.Join(strings.Split(tissue, "and"), ";"), " ", "")
			mixedSingle = "Mixed"
		} else if strings.Contains(tissue, "&") {
			tissues = strings.Split(tissue, "&")
			tissue = strings.ReplaceAll(strings.Join(strings.Split(tissue, "&"), ";"), " ", "")
			mixedSingle = "Mixed"
		} else if strings.Contains(tissue, ",") {
			tissues = strings.Split(tissue, ",")
			tissue = strings.ReplaceAll(strings.Join(strings.Split(tissue, ","), ";"), " ", "")
			mixedSingle = "Mixed"
		}

		for rowIndex, row := range trayLayout {
			rowLetter := row[0]
			for colIndex, cell := range row[1:] {
				if cell != "" {
					lane := colIndex + 1
					chamber := 12*rowIndex + lane
					well := fmt.Sprintf("%s%d", rowLetter, lane)
					fishNum := cell

					trayNum, err := strconv.Atoi(trayNumber)
					logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.PANIC_ERR)

					trayExistsContinue := false
					if trayExists, err := CheckTrayNumberExists(trayNum); err == nil {
						if trayExists {
							logger.LogMessage("Tray number exists, do you want to insert anyway?")
							fmt.Scanln(&trayExistsContinue)
						}
					} else {
						logger.LogFatal("Unable to check tray number: ", err)
					}

					if trayExists, err := CheckTrayNameExists(trayName); err == nil {
						if trayExists {
							logger.LogMessage("Tray name exists, do you want to insert anyway?")
							fmt.Scanln(&trayExistsContinue)
						}
					} else {
						logger.LogFatal("Unable to check tray name: ", err)
					}

					newTrayRecord := json_mgl.MetadataTray{
						TrayNumber:  &trayNum,
						TrayName:    &trayName,
						Chamber:     &chamber,
						Lane:        &lane,
						Well:        &well,
						Row:         &rowLetter,
						Column_n:    &lane,
						MixedSingle: &mixedSingle,
						Tissue:      &tissue,
						Species:     &species,
						RunBy:       &runBy,
					}

					err, isControl := object_processing_control.ControlExists(fishNum)
					if err != nil {
						logger.LogError("Error getting control status: ", err.Error())
					}

					if !isControl {
						specimenID, ednaID, correctName, err := object_processing_edna_specimen_ids.CheckEdnaAndSpecimenIdMatch(fishNum)
						if err != nil {
							logger.LogWarning(err.Error())
							newTrayRecord.FishNum = &fishNum
							invalidTrayRecords = append(invalidTrayRecords, newTrayRecord)
							continue
						}

						if specimenID != -1 {
							newTrayRecord.TraySpecimenID = &specimenID

						} else if ednaID != -1 {
							newTrayRecord.EdnaID = &ednaID
						}

						if correctName != "" {
							newTrayRecord.AlternateNum = &fishNum
							newTrayRecord.FishNum = &correctName
						} else {
							newTrayRecord.FishNum = &fishNum
						}
					}

					validTrayRecords = append(validTrayRecords, newTrayRecord)
				}
			}

		}
	}

	return invalidTrayRecords, validTrayRecords
}
