package object_processing_x

import (
	"encoding/json"

	"gitlab.com/UrsusArcTech/logger"
)

func RemoveEntryFields[T any](structToGet T, fieldsToExclude []string) []byte {
	// Convert struct to map
	var dataMap map[string]interface{}
	inrec, _ := json.Marshal(structToGet)
	err := json.Unmarshal(inrec, &dataMap)
	if err != nil {
		logger.LogError(err.Error())
	}

	// Remove unwanted fields
	for _, field := range fieldsToExclude {
		delete(dataMap, field)
	}

	// Convert map back to JSON
	entryMarsh, err := json.Marshal(dataMap)
	if err != nil {
		logger.LogError(err.Error())
	}

	return entryMarsh
}
