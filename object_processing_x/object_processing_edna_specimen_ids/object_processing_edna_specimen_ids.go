package object_processing_edna_specimen_ids

import (
	"errors"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_edna"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_specimen"
)

func CheckEdnaAndSpecimenIdMatch(id string) (specimenID int, ednaID int, correctName string, error error) {
	if ednaID == -1 {
		logger.LogError("Invalid eDNA ID")
		return -1, -1, "", errors.New("Invalid eDNA ID")

	}

	if specimenID == -1 {
		logger.LogError("Invalid specimen NA ID")
		return -1, -1, "", errors.New("Invalid specimen ID")
	}

	// if the ID exists in the alt map, then pass that into the ID cleaner
	if altID := object_processing_specimen.GetCorrectIdFromAltMap(id); altID != "" {
		logger.LogDebugMessage("Found "+altID+" in alt map.", "random_forest.go")
		specimenID, correctName = object_processing_specimen.GetSpecimenID(altID)
		ednaID, correctName = object_processing_edna.GetEDNAID(altID)
	} else {
		//otherwise just put the raw ID in there
		specimenID, correctName = object_processing_specimen.GetSpecimenID(id)
		ednaID, correctName = object_processing_edna.GetEDNAID(id)
	}

	if specimenID != -1 && ednaID != -1 {
		return specimenID, ednaID, "", errors.New("eDNA AND specimen ID !!BOTH MATCH!! for: " + id)
	}

	if specimenID == -1 && ednaID == -1 {
		return -1, -1, "", errors.New("eDNA AND specimen ID NOT found for: " + id)
	}

	return specimenID, ednaID, correctName, nil
}

/*

func SetEdnaAndSpecimenId(id string, eDNAID *int, specimenID *int, correctName *string, altName *string) error {
	spID := -1
	eID := -1
	corrName := ""

	// if the ID exists in the alt map, then pass that into the ID cleaner
	if altID := object_processing_specimen.GetCorrectIdFromAltMap(id); altID != "" {
		logger.LogDebugMessage("Found "+altID+" in alt map.", "random_forest.go")
		spID, corrName = object_processing_specimen.GetSpecimenID(altID)
		eID, corrName = object_processing_edna.GetEDNAID(altID)
	} else {
		//otherwise just put the raw ID in there
		spID, corrName = object_processing_specimen.GetSpecimenID(id)
		eID, corrName = object_processing_edna.GetEDNAID(id)
	}

	if spID != -1 && eID != -1 {
		eDNAID = nil
		specimenID = nil
		return errors.New("eDNA AND specimen ID !!BOTH MATCH!! for: " + id)
	}

	if spID == -1 && eID == -1 {
		eDNAID = nil
		specimenID = nil
		return errors.New("eDNA AND specimen ID NOT found for: " + id)
	}

	return nil
}
*/
