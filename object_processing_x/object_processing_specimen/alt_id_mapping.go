package object_processing_specimen

import (
	"fmt"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// this will allow us to have a file that specifies IDs that are not within the import file.
// E.g. if an import is tag IDs instead of fish IDs, we can map that.
var altIDMap map[string]string

func init() {
	altIDMap = make(map[string]string)
}

func GetCorrectIdFromAltMap(altID string) string {
	return altIDMap[altID]
}

func LoadAltMapIdWithPath(filePath string) error {
	if xlsx, err := excelize.OpenFile(filePath); err == nil {
		return loadAltMapFromXl(xlsx)
	} else {
		return err
	}
}

// this expects just two columns, the headings should be "correct_id" and "alt_id"
func loadAltMapFromXl(xlsxFile *excelize.File) error {
	const correctID = "correct_id"
	const altID = "alt_id"

	correctIDColumn := -1
	altIDColumn := -1

	correctIDCounter := 0
	altIDCounter := 0

	firstSheetName := xlsxFile.GetSheetName(1)

	if firstSheetName == "" {
		return fmt.Errorf("Cannot find first sheet in alt map file.")
	}

	//check if header row exists
	for i, header := range xlsxFile.GetRows(firstSheetName)[0] {
		//find matching header names
		if correctID == strings.ToLower(header) {
			correctIDColumn = i
			correctIDCounter++
		}
		if altID == strings.ToLower(header) {
			altIDColumn = i
			altIDCounter++
		}
		//if both are found then break the loop
		if correctIDColumn > 1 && altIDColumn > 1 {
			fmt.Println("Found correct and alt columns.")
			//if there are more than one of each then tell the user
			if correctIDCounter > 0 || altIDCounter > 0 {
				fmt.Println("ERROR: There were more than one columns for 'correct_id' or 'alt_id'. Continuing with the last found columns.")
				fmt.Printf("correct_id column count: %d. alt_id column count: %d\n", correctIDCounter, altIDCounter)
				fmt.Println("Continuing anyway...")
			}
			break
		}
	}

	if correctIDColumn < 0 || altIDColumn < 0 {
		return fmt.Errorf("correct_id and/or alt_id column not found in the alternate ID file, so no alt map will be used. Expecting just two columns, the headings should be 'correct_id' and 'alt_id'")
	}

	//this looks at the first sheet and maps the alts to the correct IDs
	for _, column := range xlsxFile.GetRows(firstSheetName)[1:] {
		//if this map entry is not empty, warn and overwrite it
		if altIDMap[column[altIDColumn]] != "" {
			fmt.Printf("%s is duplicated in the map, overwriting its value...\n", column[altIDColumn])
		}
		//map alt to correct ID
		fmt.Printf("Mapping: %s to: %s\n", column[altIDColumn], column[correctIDColumn])
		altIDMap[column[altIDColumn]] = column[correctIDColumn]
	}

	return nil
}
