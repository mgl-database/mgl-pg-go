package object_processing_specimen

import (
	"regexp"
	"strings"
)

func CleanIds(ID string) string {
	re := regexp.MustCompile("[\\s\\xA0]+")
	ID = re.ReplaceAllString(ID, "")
	ID = strings.ToLower(ID)
	return ID
}
