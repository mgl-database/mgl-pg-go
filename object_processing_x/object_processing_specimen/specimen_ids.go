package object_processing_specimen

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	"gitlab.com/mgl-database/mgl-pg-go/map_x"
)

// custom errors to automate logging and error raising
var cannotFindFishError = errors.New("sh.")
var notUniqueFishError = errors.New("Not a unique fish num.")

// cache so that we can minimize API calls and a lock for thread safety
var fishIdCache = make(map[string]int)

func init() {
	thread_safe_error_handling.Errors.AddCustomError(cannotFindFishError, thread_safe_error_handling.WARN_ERR)
	thread_safe_error_handling.Errors.AddCustomError(notUniqueFishError, thread_safe_error_handling.WARN_ERR)
}

// check if specimen is unique by checking the count
func IsSpecimenUnique(fish_num string) (bool, error) {

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	//get http response for specimen count
	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/get_fish_id_count?fish_num="+fish_num, json_net_x.PATCH, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return false, err
	}

	//unmarshal
	var specimenCount json_mgl.SpecimenCount
	jsonErr := json.Unmarshal(responseData, &specimenCount)

	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return false, jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return false, errors.New(string(responseData))
	}

	//check count
	if specimenCount[0].Count == 1 {
		return true, nil
	} else if specimenCount[0].Count > 1 {
		return false, nil
	} else {
		return false, errors.New("Could not find specimen.")
	}
}

// get specimen ID using the fish num
func getSpecimenIDUsingFishID(fish_num string) (fishID int, fishNum string, err error) {

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	//if not in cache, check API
	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-specimen_metadata?fish_num="+fish_num, json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return -1, "", err
	}

	var specimen []json_mgl.MetadataSpecimen
	jsonErr := json.Unmarshal(responseData, &specimen)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return -1, "", jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return -1, "", errors.New(string(responseData))
	}

	//check if thre is a fish returned
	if len(specimen) < 1 {
		logger.LogDebugMessage("Cannot find fish: "+fish_num, "")
		return -1, "", cannotFindFishError
	}

	//check if there is more than one fish returned
	if len(specimen) > 1 {
		logger.LogError("Not unique: "+fish_num, "")
		return -1, "", notUniqueFishError
	}

	//if we've found the fish, write it to the cache
	map_x.WriteMapStrInt(fishIdCache, fish_num, specimen[0].SpecimenSpecimenID)

	//return the specimen found
	return specimen[0].SpecimenSpecimenID, specimen[0].FishNum, nil
}

// same but with unique ID instead
func getSpecimenByUniqueID(uniqueID string) (fishID int, fishNum string, err error) {

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	//if not in cache, check API
	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-specimen_metadata?specimen_unique_id="+uniqueID, json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return -1, "", err
	}

	var specimen []json_mgl.MetadataSpecimen
	jsonErr := json.Unmarshal(responseData, &specimen)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return -1, "", jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return -1, "", errors.New(string(responseData))
	}

	//check if there is a fish returned
	if len(specimen) < 1 {
		logger.LogDebugMessage("Cannot find fish: "+uniqueID, "")
		return -1, "", cannotFindFishError
	}

	//check if there is more than one fish returned
	if len(specimen) > 1 {
		logger.LogError("Not unique: "+uniqueID, "")
		return -1, "", notUniqueFishError
	}

	//if we've found the fish, write it to the cache
	map_x.WriteMapStrInt(fishIdCache, uniqueID, specimen[0].SpecimenSpecimenID)

	//return the specimen found
	return specimen[0].SpecimenSpecimenID, specimen[0].FishNum, nil
}

func getSpecimenByRegex(name string) (fishID int, fishNum string) {
	//try basic alpha numeric
	pattern := regexp.MustCompile(`[A-Z]+\d+`)
	id := pattern.FindString(name)

	fishIdRegEx, fishNum, err := getSpecimenIDUsingFishID(id)

	//check for error, doesn't matter if it's true/false, we just want the logger and to check against the custom error
	_ = logger.CheckForCustomError(err, cannotFindFishError)

	if fishIdRegEx != -1 {
		logger.LogDebugMessage("Fish: "+name+", using: "+id+", matched to regex:"+strconv.Itoa(fishIdRegEx), "")
		return fishIdRegEx, fishNum
	}

	//try removing the FW--- prefix
	re := regexp.MustCompile(`FW[0-9]{1,2}[HNMS]\_(.*)$`)
	match := re.FindStringSubmatch(name)
	if len(match) > 1 {
		fishIdRegEx2, fishNum, err := getSpecimenIDUsingFishID(match[1])

		_ = logger.CheckForCustomError(err, cannotFindFishError)

		if fishIdRegEx2 != -1 {
			logger.LogDebugMessage("Fish: "+name+", using: "+id+", matched to regex:"+strconv.Itoa(fishIdRegEx), "")
			return fishIdRegEx2, fishNum
		}
	}
	return -1, ""
}

// this will try and find a specimen name by checking if it's a basic id, then unique id, then will try a regex
func GetSpecimenID(name string) (specimenID int, specimenName string) {
	//check cache
	if isAssigned, val := map_x.GetMapAssignmentStrInt(fishIdCache, name); isAssigned {
		logger.LogDebugMessage("Found  ID: "+name+" in cache, value: "+strconv.Itoa(val), "")
		return val, ""
	}

	//see if the fish can be found with just its name
	fishID, fishNum, err := getSpecimenIDUsingFishID(name)

	//check for error, doesn't matter if it's true/false, we just want the logger and to check against the custom error
	_ = logger.CheckForCustomError(err, cannotFindFishError)

	if fishID != -1 {
		logger.LogDebugMessage("Fish: "+name+", using: "+name+", matched to basic fish ID:"+strconv.Itoa(fishID), "")
		return fishID, fishNum
	}

	//if specimen name is a unique id, get that id
	fishUniqueID, fishNum, err := getSpecimenByUniqueID(name)

	//check for error, doesn't matter if it's true/false, we just want the logger and to check against the custom error
	logger.CheckForCustomError(err, cannotFindFishError)

	if fishUniqueID != -1 {
		logger.LogDebugMessage("Fish: "+name+", using: "+name+", matched to unique ID:"+strconv.Itoa(fishUniqueID), "")
		return fishUniqueID, fishNum
	}

	if regspid, fishNum := getSpecimenByRegex(name); regspid != -1 {
		return regspid, fishNum
	}

	//if any still have no ID, present to user and manually fix
	foundID := false
	for !foundID {
		input := ""
		//	logger.LogError(fmt.Sprintf("Cannot match ID for: -%v-", name), "")
		//fmt.Println(fmt.Sprintf("Cannot match ID for: %v, what is it? Type 'control' to ignore it.\n", name))
		map_x.WriteMapStrInt(fishIdCache, name, -1)
		return -1, ""
		//fmt.Scanln(&input)

		if input == "control" {
			break
		}

		fishIdManual, fishNum, err := getSpecimenIDUsingFishID(input)

		if err != nil {
			fmt.Println(err)
		}

		if fishIdManual != -1 {
			return fishIdManual, fishNum
		}
	}

	return -1, ""
}
