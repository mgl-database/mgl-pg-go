package object_processing_edna

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	generic_api_interactions "gitlab.com/mgl-database/mgl-pg-go/json_types/db/generic"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	"gitlab.com/mgl-database/mgl-pg-go/map_x"
)

// custom errors to automate logging and error raising
var cannotFindEDNAError = errors.New("Cannot find eDNA.")
var notUniqueEDNAError = errors.New("Not a unique edna num.")

// cache so that we can minimize API calls and a lock for thread safety
var eDNAIdCache = make(map[string]int)

// this is for when there is no way to match the fish, if we want to skip all checking we can turn off the checks here
var skipFishIdEntry = false

func init() {
	thread_safe_error_handling.Errors.AddCustomError(cannotFindEDNAError, thread_safe_error_handling.WARN_ERR)
	thread_safe_error_handling.Errors.AddCustomError(notUniqueEDNAError, thread_safe_error_handling.WARN_ERR)
}

// check if specimen is unique by checking the count
func IsEDNAUnique(fish_num string) (bool, error) {

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	//get http response for specimen count
	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/get_edna_id_count?edna_id="+fish_num, json_net_x.PATCH, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return false, err
	}

	//unmarshal
	var ednaCount generic_api_interactions.Counts
	jsonErr := json.Unmarshal(responseData, &ednaCount)

	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return false, jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return false, errors.New(string(responseData))
	}

	//check count
	if ednaCount[0].Count == 1 {
		return true, nil
	} else if ednaCount[0].Count > 1 {
		return false, nil
	} else {
		return false, errors.New("Could not find eDNA.")
	}
}

// get edna ID using basic eDNA ID
func getEDNAIDUsingBasicID(edna_id string) (ednaID int, ednaName string, e error) {
	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}
	//if not in cache, check API
	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-edna_metadata?sample_id="+edna_id, json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return -1, "", err
	}

	var eDNA []json_mgl.MetadataEdna
	jsonErr := json.Unmarshal(responseData, &eDNA)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return -1, "", jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return -1, "", errors.New(string(responseData))
	}

	//check if thre is a edna record returned
	if len(eDNA) < 1 {
		logger.LogDebugMessage("Cannot find eDNA: "+edna_id, "")
		return -1, "", cannotFindEDNAError
	}

	//check if there is more than one fish returned
	if len(eDNA) > 1 {
		logger.LogError("Not unique: "+edna_id, "")
		return -1, "", notUniqueEDNAError
	}

	//if we've found the fish, write it to the cache
	map_x.WriteMapStrInt(eDNAIdCache, edna_id, eDNA[0].ID)

	//return the eDNA record found
	return eDNA[0].ID, *eDNA[0].SampleID, nil
}

// same but with unique ID instead
func getEDNAByUniqueID(uniqueID string) (ednaID int, ednaName string, e error) {

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	//if not in cache, check API
	responseData, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-edna_metadata?unique_id="+uniqueID, json_net_x.GET, nil)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return -1, "", err
	}

	var eDNA []json_mgl.MetadataEdna
	jsonErr := json.Unmarshal(responseData, &eDNA)
	if logger.CheckForErrorWithLevel(jsonErr, nil, thread_safe_error_handling.PANIC_ERR) {
		return -1, "", jsonErr
	}

	if strings.Contains(string(responseData), "error") {
		logger.LogError(string(responseData), "")
		return -1, "", errors.New(string(responseData))
	}

	//check if there is a fish returned
	if len(eDNA) < 1 {
		logger.LogDebugMessage("Cannot find eDNA: "+uniqueID, "")
		return -1, "", cannotFindEDNAError
	}

	//check if there is more than one fish returned
	if len(eDNA) > 1 {
		logger.LogError("Not unique eDNA: "+uniqueID, "")
		return -1, "", notUniqueEDNAError
	}

	//if we've found the fish, write it to the cache
	map_x.WriteMapStrInt(eDNAIdCache, uniqueID, eDNA[0].ID)

	//return the specimen found
	return eDNA[0].ID, *eDNA[0].SampleID, nil
}

func getEDNAByRegex(name string) (ednaID int, eDNAname string) {
	//try basic alpha numeric
	pattern := regexp.MustCompile(`[A-Z]+\d+`)
	id := pattern.FindString(name)

	fishIdRegEx, eDNAname, err := getEDNAIDUsingBasicID(id)

	//check for error, doesn't matter if it's true/false, we just want the logger and to check against the custom error
	logger.CheckForCustomError(err, cannotFindEDNAError)

	if fishIdRegEx != -1 {
		logger.LogDebugMessage("eDNA: "+name+", using: "+id+", matched to regex:"+strconv.Itoa(fishIdRegEx), "")
		return fishIdRegEx, eDNAname
	}

	//try removing the FW--- prefix
	re := regexp.MustCompile(`FW[0-9]{1,2}[HNMS]\_(.*)$`)
	match := re.FindStringSubmatch(name)
	if len(match) > 1 {
		fishIdRegEx2, eDNAname, err := getEDNAIDUsingBasicID(match[1])

		_ = logger.CheckForCustomError(err, cannotFindEDNAError)

		if fishIdRegEx2 != -1 {
			logger.LogDebugMessage("eDNA: "+name+", using: "+id+", matched to regex:"+strconv.Itoa(fishIdRegEx), "")
			return fishIdRegEx2, eDNAname
		}
	}
	return -1, ""
}

// this will try and find a eDNA name by checking if it's a basic id, then unique id, then will try a regex
func GetEDNAID(name string) (ednaID int, ednaName string) {
	//check cache
	if isAssigned, val := map_x.GetMapAssignmentStrInt(eDNAIdCache, name); isAssigned {
		logger.LogDebugMessage("Found  ID: "+name+" in cache, value: "+strconv.Itoa(val), "")
		return val, ""
	}

	//see if the fish can be found with just its name
	ednaID, ednaName, err := getEDNAIDUsingBasicID(name)

	//check for error, doesn't matter if it's true/false, we just want the logger and to check against the custom error
	_ = logger.CheckForCustomError(err, cannotFindEDNAError)

	if ednaID != -1 {
		logger.LogDebugMessage("eDNA: "+name+", using: "+name+", matched to basic eDNA ID:"+strconv.Itoa(ednaID), "")
		return ednaID, ednaName
	}

	//if specimen name is a unique id, get that id
	fishUniqueID, ednaName, err := getEDNAByUniqueID(name)

	//check for error, doesn't matter if it's true/false, we just want the logger and to check against the custom error
	_ = logger.CheckForCustomError(err, cannotFindEDNAError)

	if fishUniqueID != -1 {
		logger.LogDebugMessage("eDNA: "+name+", using: "+name+", matched to unique ID:"+strconv.Itoa(fishUniqueID), "")
		return fishUniqueID, ednaName
	}

	if regspid, ednaName := getEDNAByRegex(name); regspid != -1 {
		return regspid, ednaName
	}

	//if any still have no ID, present to user and manually fix
	foundID := false
	for !foundID {
		input := ""
		//logger.LogError(fmt.Sprintf("Cannot match eDNA ID for: -%v-", name), "")
		//fmt.Println(fmt.Sprintf("Cannot match ID for: %v, what is it? Type 'control' to ignore it.\n", name))
		map_x.WriteMapStrInt(eDNAIdCache, name, -1)
		return -1, ""
		//fmt.Scanln(&input)

		if input == "control" {
			break
		}

		ednaIdManual, ednaName, err := getEDNAIDUsingBasicID(input)

		if err != nil {
			fmt.Println(err)
		}

		if ednaIdManual != -1 {
			return ednaIdManual, ednaName
		}
	}

	return -1, ""
}
