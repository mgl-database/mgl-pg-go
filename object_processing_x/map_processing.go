package object_processing_x

// this is needed so that we can find the key because now we want to store the alternative names they put down....
func reverseMapSearch(m map[string]int, value int) (key string) {
	for k, v := range m {
		if v == value {
			key = k
			return
		}
	}
	return
}
