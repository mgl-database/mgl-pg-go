package io_xl

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	//	cleanids "gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_random_forest_data/clean_ids"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/io_x"
	//	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_random_forest_data/api_interactions/object_processing"
)

// function to override to use for threading
type XLSXProcessor func(filename string, outputdir string, projectID int, importGroupID int, wg *sync.WaitGroup)

/*
Just a function to loop through xlsx files and create new threads
Make sure to define the processor, e.g.:

	var myFunc xl.XLSXProcessor = func(filename string, outputdir string, projectID int, importGroupID int, wg *sync.WaitGroup) {
		logger.LogWarning(filename)
		wg.Done()
	}
	xl.ThreadDirectoryXLSXFiles("/home/a/Downloads/G", "/home/a/Downloads/G", 0, 0, myFunc)
*/
func ThreadDirectoryXLSXFiles(inputdir string, outputdir string, projectID int, importGroupID int, xlsxProcessor XLSXProcessor) {
	var wg sync.WaitGroup

	if xlsxProcessor == nil {
		logger.LogFatal("XLSXProcessor not defined.")
	}

	files, err := os.ReadDir(inputdir)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if strings.HasSuffix(file.Name(), ".xlsx") && !strings.HasPrefix(file.Name(), "~lock.") {
			//put on new thread
			wg.Add(1)
			go xlsxProcessor(filepath.Join(inputdir, file.Name()), outputdir, projectID, importGroupID, &wg)
		}
	}
	wg.Wait()
}

func WriteCSV(outputFileAndDir string, selectedClassifier string, rows [][]string) {
	splitPath := strings.Split(outputFileAndDir, "/")
	splitPath = splitPath[0 : len(splitPath)-1]

	var parentDir string
	for i, elem := range splitPath {
		if i == len(splitPath) {
			break
		}
		parentDir += elem + "/"
	}

	fmt.Println(parentDir)

	//make output dir
	if dirErr := io_x.MakeOutputDir(parentDir); dirErr != nil {
		logger.LogError("Could not make directory during file: "+outputFileAndDir, "")
		return
	}

	csvFile, err := os.Create(outputFileAndDir)
	if err != nil {
		log.Fatal(err)
	}
	defer csvFile.Close()

	csvWriter := csv.NewWriter(csvFile)

	//write csv
	for i, row := range rows {
		if i == 0 {
			row = append(row, "classifier")
		} else {
			row = append(row, selectedClassifier)
		}
		err := csvWriter.Write(row)
		if err != nil {
			log.Fatal(err)
		}
	}
	csvWriter.Flush()
}
