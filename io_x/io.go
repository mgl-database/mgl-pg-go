package io_x

import (
	"os"

	"gitlab.com/UrsusArcTech/logger"
)

func MakeOutputDir(outputDir string) error {
	//make output folder
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		err = os.Mkdir(outputDir, 0755)
		if err != nil {
			logger.LogWarning("Could not make directory: "+outputDir, "")
			return err
		}
	}
	return nil
}
