# MGL-PG-API Monolith

This is the monolith for all API apps for the MGL database.

# Project Info Inside Monoloth

## 1. SEP
### Dedug database deletion:

```sh
truncate 
sep.metadata_fish,
sep.metadata_fish_sampling_type_group,
sep.metadata_edna,
sep.metadata_edna_sampling_type_group,
sep.metadata_fishingset,
sep.current_cohorts_link,
sep.cohort_link,
sep.metadata_cohort,
sep.metadata_hatchery,
sep.epi_aq,
sep.daily_data,
sep.daily_aq,
sep.daily_hatchery
;
```

## 2. WCVI-Microtrolling

This is the FTF microtrolling API bridge.

## 3. pg_mgl_normalized_data

This is the conversion to normalized data from the biomark format. This will eventually be hooked up to the API.

## 4. pg_random_forest_data

This is the fully integrated API processing of random forest data.