module gitlab.com/mgl-database/mgl-pg-go

go 1.22.3

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	gitlab.com/UrsusArcTech/logger v0.0.0-20241015014501-9d21217542ec
)

require github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
