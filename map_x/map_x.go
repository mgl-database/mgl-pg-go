package map_x

import (
	"sync"
)

var strIntLock = sync.RWMutex{}
var intStrLock = sync.RWMutex{}

func FindKeyByValue[Key comparable, Val comparable](m map[Key]Val, value Val) (Key, bool) {
	var zero Key
	for k, v := range m {
		if v == value {
			return k, true
		}
	}
	return zero, false
}

// thread safe map reads, etc.
func ReadMap[Key comparable, Val comparable](mapToCheck map[Key]Val, key Key) Val {
	strIntLock.Lock()
	defer strIntLock.Unlock()
	return mapToCheck[key]
}

func WriteMapStrInt(mapToCheck map[string]int, key string, value int) {
	strIntLock.Lock()
	defer strIntLock.Unlock()
	mapToCheck[key] = value
}

func GetMapAssignmentStrInt(mapToCheck map[string]int, key string) (isAssigned bool, val int) {
	strIntLock.Lock()
	defer strIntLock.Unlock()
	val, isAssigned = mapToCheck[key]
	return isAssigned, val
}

func WriteMapIntStr(mapToCheck map[int]string, key int, value string) {
	intStrLock.Lock()
	defer intStrLock.Unlock()
	mapToCheck[key] = value
}

func GetMapAssignmentIntStr(mapToCheck map[int]string, key int) (isAssigned bool, val string) {
	intStrLock.Lock()
	defer intStrLock.Unlock()
	val, isAssigned = mapToCheck[key]
	return isAssigned, val
}

func GetMapAssignmentIntInt(mapToCheck map[int]int, key int) (isAssigned bool, val int) {
	intStrLock.Lock()
	defer intStrLock.Unlock()
	val, isAssigned = mapToCheck[key]
	return isAssigned, val
}
