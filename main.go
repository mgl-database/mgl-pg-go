package main

import (
	"fmt"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/ftf/wcvi_microtrolling"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_mgl_gsi"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_mgl_normalized_data"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_mgl_tray_data"
	"gitlab.com/mgl-database/mgl-pg-go/project_interfaces/pg_mgl/pg_random_forest_data"
	sep_bridge "gitlab.com/mgl-database/mgl-pg-go/project_interfaces/sep_bridge"
)

type projectInterface struct {
	Name    string
	RunFunc func()
}

var projectInterfaces = []projectInterface{
	{
		Name:    "pg_mgl_normalized_data",
		RunFunc: pg_mgl_normalized_data.Run,
	},
	{
		Name:    "pg_mgl_random_forest_data",
		RunFunc: pg_random_forest_data.Run,
	},
	{
		Name:    "pg_mgl_tray_data",
		RunFunc: pg_mgl_tray_data.Run,
	},
	{
		Name:    "pg_mgl_gsi",
		RunFunc: pg_mgl_gsi.Run,
	},
	{
		Name:    "epi_wcvi_microtrolling",
		RunFunc: wcvi_microtrolling.Run,
	},
	{
		Name:    "sep_bridge",
		RunFunc: sep_bridge.Run,
	},
}

func selectProjectInterface(selectedProjectInterface int) int {
	projectSelected := false

	if selectedProjectInterface != -1 {
		for pid, projstr := range projectInterfaces {
			if selectedProjectInterface == pid {
				fmt.Printf("\nRunning: %s...\n\n", projstr.Name)
				projectSelected = true
				return selectedProjectInterface
			}
		}
	}

	for {
		fmt.Println("\n\nSelect which one to use:")
		for pid, projstr := range projectInterfaces {
			fmt.Printf("%d: %s\n", pid, projstr.Name)
		}
		fmt.Scanln(&selectedProjectInterface)

		for pid, projstr := range projectInterfaces {
			if selectedProjectInterface == pid {
				fmt.Printf("\nRunning: %s...\n\n", projstr.Name)
				projectSelected = true
				break
			}
		}

		if projectSelected {
			break
		}
		fmt.Print("\nInvalid selection.\n\n")
	}

	return selectedProjectInterface
}

func main() {
	selectedProjectInterface := -1

	logLevel := flags.CreateFlag[int]("-l", 7, "Set the logging level.")
	projectInterfaceID := flags.CreateFlag[int]("-pri", -1, "Project interface to select.")

	logger.SetVerbosityLevel(logger.Verbosity(logLevel))
	flags.CheckHelpFlag()

	if projectInterfaceID != -1 {
		flags.DeleteFlag("-pri")
	}

	//	for {
	selectedProjectInterface = selectProjectInterface(projectInterfaceID)

	projectInterfaces[selectedProjectInterface].RunFunc()

	projectInterfaceID = -1
	//}
}
