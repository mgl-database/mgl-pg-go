package init

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/flags"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
	json_mgl "gitlab.com/mgl-database/mgl-pg-go/json_types/db/mgl"
	"gitlab.com/mgl-database/mgl-pg-go/object_processing_x/object_processing_specimen"
)

func getProjectID(proj_id int) int {
	var project_id_before_test string

	dfoUrl, err := flags.GetFlagVal[string]("-geturl")
	if err != nil {
		logger.LogFatal("DFO URL isn't set.")
	}

	if proj_id == -99 {
		fmt.Println("------------------------------------")
		fmt.Println("What is the project id? Type 'list' to see project list")
		fmt.Scanln(&project_id_before_test)
	} else {
		project_id_before_test = strconv.Itoa(proj_id)
	}

	var programs json_mgl.Program
	finalval := 0

	if project_id_before_test == "list" {
		//print all project IDs
		prog_raw, err := json_net_x.HttpResponseApiRequest(dfoUrl+"mgl/mgl-programs", json_net_x.GET, nil)
		if err != nil {
			fmt.Println(err)
			finalval = getProjectID(proj_id)
		}
		json.Unmarshal(prog_raw, &programs)

		for _, program := range programs {
			fmt.Println(fmt.Sprintf("ID: %v | %v | %v", program.Program_id, program.Program_short, program.Program_short))
		}
		finalval = getProjectID(proj_id)
	} else if val, err := strconv.Atoi(project_id_before_test); err == nil {
		if val > 0 {
			return val
		} else {
			fmt.Println("Project ID must be more than 0.")
			finalval = getProjectID(proj_id)
		}
	} else {
		fmt.Println(err)
		finalval = getProjectID(proj_id)
	}
	return finalval
}

func getDirectory(inputDirectory string) string {
	finalDir := ""

	if inputDirectory == "" {
		fmt.Println("------------------------------------")
		fmt.Println("What is the import path?")
		fmt.Scanln(&inputDirectory)
	}

	if dir, err := os.ReadDir(inputDirectory); err == nil {
		fmt.Println("Found files in directory:")
		for _, files := range dir {
			fmt.Println(files.Name())
		}
		return inputDirectory

	} else {
		fmt.Println("Directory error: ", err)
		finalDir = getDirectory("")
	}
	return finalDir
}

func getAltIDMap(filePath string) {

	if filePath == "" {
		fmt.Println("------------------------------------")
		fmt.Println("Where is the alternate ID map? (type 'n' to skip)")
		fmt.Scanln(&filePath)
	}

	if filePath == "n" {
		fmt.Println("Will not use an alt map.")
		return
	}

	if err := object_processing_specimen.LoadAltMapIdWithPath(filePath); err != nil {
		fmt.Println(err)
		getAltIDMap(filePath)
	}
}
