package json_net_x

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
)

type HttpRequests string

const (
	PATCH HttpRequests = "PATCH"
	GET   HttpRequests = "GET"
	POST  HttpRequests = "POST"
)

// this just does an api request and any returns are just a string
func BlindApiRequest(url string, httpRequestType HttpRequests, requestBody io.Reader) (string, error) {
	url = strings.ReplaceAll(url, " ", "")
	req, err := http.NewRequest(string(httpRequestType), url, requestBody)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return "", err
	}

	// Use http.DefaultClient or create a custom http.Client to send the request
	resp, err := http.DefaultClient.Do(req)
	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return "", err
	}

	responseData, err := io.ReadAll(resp.Body)
	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return "", err
	}

	logger.LogDebugMessage("API response: "+string(responseData), "")

	if strings.Contains(string(responseData), "400") {
		fmt.Println("Bad req")
	}

	defer resp.Body.Close()

	if strings.Contains(string(responseData), "error") {
		return "", fmt.Errorf("BLIND API ERROR: %s", string(responseData))
	}

	return string(responseData), nil
}

// this is an api request that returns a http response object
func HttpResponseApiRequest(url string, httpRequestType HttpRequests, requestBody io.Reader) ([]byte, error) {
	url = strings.ReplaceAll(url, " ", "")
	var responseBodyNil []byte
	req, err := http.NewRequest(string(httpRequestType), url, requestBody)

	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return responseBodyNil, err
	}

	// Use http.DefaultClient or create a custom http.Client to send the request
	resp, err := http.DefaultClient.Do(req)
	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return responseBodyNil, err
	}

	responseData, err := io.ReadAll(resp.Body)
	if logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.WARN_ERR) {
		return responseBodyNil, err
	}

	logger.LogDebugMessage("API response: "+string(responseData), "")

	if strings.Contains(string(responseData), "Request") {
		fmt.Println("Bad req")
	}

	defer resp.Body.Close()

	if strings.Contains(string(responseData), "error") {
		return nil, fmt.Errorf("HTTP RESPONSE API ERROR: %s", string(responseData))
	}

	return responseData, nil
}

func UnmarshalURL(url string, anyStruct interface{}, httpMethod string) error {
	// Create a new request using http
	req, err := http.NewRequest(httpMethod, url, nil)
	if err != nil {
		logger.LogError(err.Error())
		return err
	}

	// Send req using http Client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.LogError(err.Error())
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.LogError(err.Error())
		return err
	}

	err = json.Unmarshal(body, &anyStruct)
	if err != nil {
		logger.LogError(err.Error())
		return err
	}

	return nil
}
