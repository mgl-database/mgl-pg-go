package json_net_epi

// set up all of the connection things, such as getting the access token, etc.

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

type Token struct {
	AccessToken string `json:"access_token"`
}

var token Token

// should probably add a check to see if the token is still valid and if not run init again
func GetToken() string {
	if token.AccessToken == "" {
		Logging.LogError("Access token is null.")
	}
	return token.AccessToken
}

// this just gets the token at the start
func SetToken(clientID int, clientSecret string) {
	params := map[string]interface{}{
		"grant_type":    "client_credentials",
		"client_id":     clientID,
		"client_secret": clientSecret,
	}

	jsonParams, _ := json.Marshal(params)

	req, _ := http.NewRequest("POST", "https://five.epicollect.net/api/oauth/token", bytes.NewBuffer(jsonParams))
	fmt.Println(string(jsonParams))
	req.Header.Add("Content-Type", "application/vnd.api+json")

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		Logging.LogError(err.Error())
		panic(err.Error())
	}

	if res.StatusCode != http.StatusOK {
		Logging.LogError(res.Status)
		panic(res.Status)
	}

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	var newToken Token
	unmarshalErr := json.Unmarshal(body, &newToken)

	if unmarshalErr != nil {
		Logging.LogError("Error unmarshalling access token: " + unmarshalErr.Error())
	} else {
		token = newToken
	}
}

// This is a generic function that gets the entry pages and combines them
func GetEntryPages[T epi.Epi](newEntries *[]T, accessToken string, url string) {

	var aqEntry T
	getEntries(accessToken, url, &aqEntry)
	*newEntries = append(*newEntries, aqEntry)

	if aqEntry.GetLinks().Next != "" {
		GetEntryPages(newEntries, accessToken, aqEntry.GetLinks().Next)
	}

	if len(*newEntries) == 0 {
		Logging.LogWarning("Entries are empty.")
	}
}

// this is a generic function to get entries that are subtypes of epi.Epi
func getEntries[T epi.Epi](token string, url string, entries *T) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		Logging.LogError(err.Error())
		return
	}

	req.Header.Add("Content-Type", "application/vnd.api+json")
	req.Header.Add("Authorization", "Bearer "+token)

	res, err := client.Do(req)

	if err != nil {
		Logging.LogError("Error getting auth token " + url + ". Error: " + err.Error())
		return
	}

	for {
		if res.StatusCode == http.StatusTooManyRequests {
			Logging.LogWarning("Hit api limit, waiting.... error: " + res.Status)
			time.Sleep(time.Second * 61)
			res, err = client.Do(req)
		} else {
			break
		}
	}

	if res.StatusCode != http.StatusOK {
		Logging.LogError("Error getting entries for: " + url + ". Status: " + res.Status)
		return
	}

	defer res.Body.Close()
	body, readErr := io.ReadAll(res.Body)

	if readErr != nil {
		Logging.LogError("Error reading body for url: " + url + ". Error: " + err.Error())
		return
	}

	err = json.Unmarshal(body, entries)
	if err != nil {
		Logging.LogError("Error unmarshaling body for url: " + url + ". Error: " + err.Error())
	}
}
