package json_net_x

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/UrsusArcTech/logger"
)

type DBPostType string

const (
	INSERT  DBPostType = "INSERT"
	UPSERT             = "UPSERT"
	DBPATCH            = "PATCH"
)

func SubmitMarshal[T interface{}](url string, dbPostType DBPostType, all T) error {
	header := make(http.Header)

	logger.LogMessage("Starting for: " + url)

	data, err := json.Marshal(all)
	if err != nil {
		logger.LogError("Error marshalling data: " + err.Error())
		return err
	}

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	if err := SubmitBytesToUrl(url, data, header); err != nil {
		logger.LogError("Error submitting bytes: ", err.Error())
		return err
	}
	logger.LogMessage("Submission done.")
	return nil
}

func SubmitBytesToUrl(url string, newBytes []byte, header http.Header) error {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(newBytes))
	if err != nil {
		logger.LogError("Error creating post request for: " + url + ". Error: " + err.Error())
		return err
	}

	req.Header = header

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.LogError("Error trying to connect to: " + url + ". Error: " + err.Error())
		return err
	}

	if resp == nil {
		logger.LogError("No response from: " + url + ". No error returned.")
		return errors.New("No response from: " + url + ". No error returned.")
	}

	if resp.StatusCode != http.StatusOK {
		rBody, readErr := io.ReadAll(resp.Body)
		if readErr != nil {
			logger.LogError("Respose body read error: " + readErr.Error())
			return err
		}
		logger.LogError("Status: " + resp.Status + ". body: " + string(rBody))
		logger.LogError(resp.Status)
		return errors.New("HTTP Response Error On Byte Submit. Status code: " + fmt.Sprint(resp.StatusCode))
	}
	defer resp.Body.Close()
	return nil
}

func SubmitParamsURL(url string, dbPostType DBPostType) {
	header := make(http.Header)

	//logger.LogMessage("Starting params URL submit for: " + url)

	header.Add("Content-Type", "application/json")
	header.Add("db_post_type", string(dbPostType))

	req, err := http.NewRequest(string(dbPostType), url, nil)
	if err != nil {
		logger.LogError("Error creating post request for: " + url + ". Error: " + err.Error())
		return
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.LogError("Error trying to connect to: " + url + ". Error: " + err.Error())
		return
	}

	if resp.StatusCode != http.StatusOK {
		rBody, readErr := io.ReadAll(resp.Body)
		if readErr != nil {
			logger.LogError("Respose body read error: " + readErr.Error())
		}
		logger.LogError("Status: " + resp.Status + ". body: " + string(rBody))
		logger.LogError(resp.Status)
	}
	defer resp.Body.Close()
}

func RunScript(url string) {
	// Create a new HTTP client
	client := &http.Client{}

	// Create a PATCH request with an empty request body
	req, err := http.NewRequest(DBPATCH, url, nil)
	if err != nil {
		logger.LogError("Error creating PATCH request:" + err.Error())
		return
	}

	// Send the request
	resp, err := client.Do(req)
	if err != nil {
		logger.LogError("Error sending PATCH request:" + err.Error())
		return
	}
	defer resp.Body.Close()

	// Check the response status code
	if resp.StatusCode != http.StatusOK {
		logger.LogError("PATCH Request failed with status: " + resp.Status)
		return
	}
	logger.LogMessage("PATCH request done.")
}
