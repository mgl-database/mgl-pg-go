package sep_epi

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

type SampleType string

const (
	EDNA SampleType = "eDNA"
	GILL            = "GILL"
)

var submissionSampleType SampleType

// this will set the submission type which is kept until GetEntries is called
func SetSubmissionType(subSampleType SampleType) {
	submissionSampleType = subSampleType
}

func GetSubmissionType() SampleType {
	return submissionSampleType
}

func (a Sampling_type_form_5) GetLinks() epi.Links {
	return a.Links
}

func (h Sampling_type_form_5) GetData() interface{} {
	return h.Data
}

func (h Sampling_type_form_5) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a Sampling_type_form_5) GetEntries() []byte {
	var exclude []string
	var entries []interface{}

	if submissionSampleType == "" {
		Logging.LogError("Sample type submittion type not specified.")
		return nil
	} else if submissionSampleType == EDNA {
		exclude = []string{"tissue_sample", "time_dissections", "time_fish_anesth", "time_fish_pulled", "fish_sample_date", "fish_times_Sampler"}
	} else if submissionSampleType == GILL {
		exclude = []string{"eDNA"}
	}

	for _, v := range a.Data.Entries {
		if submissionSampleType != "" && submissionSampleType != SampleType(v.SampleType) {
			continue
		}
		// Convert the struct to a map
		var entryMap map[string]interface{}
		//marshal the entry
		inrec, _ := json.Marshal(v)
		//unmarshal the entry into the entry map
		json.Unmarshal(inrec, &entryMap)

		// Remove unwanted fields
		for _, exc := range exclude {
			delete(entryMap, exc)
		}

		// Add the map to the entries slice
		entries = append(entries, entryMap)
	}

	entryMarsh, err := json.Marshal(entries)
	if err != nil {
		Logging.LogError(err.Error())
	}

	//remove submission type so it's not carried over
	//SetSubmissionType("")
	return entryMarsh
}

type Sampling_type_form_5 struct {
	epi.Links `json:"links"`
	epi.Meta  `json:"meta"`
	Data      struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID       string            `json:"ec5_uuid"`
			Ec5ParentUUID string            `json:"ec5_parent_uuid"`
			CreatedAt     epi.TimeWTimeZone `json:"created_at"`
			UploadedAt    epi.TimeWTimeZone `json:"uploaded_at"`
			CreatedBy     string            `json:"created_by"`
			//Title            string        `json:"title"`
			SampleType string `json:"sample_Type"`
			//EDNA             int    `json:"eDNA"`
			FishTimesSampler string `json:"fish_times_Sampler"`
			FishSampleDate   string `json:"fish_sample_date"`
			TimeFishPulled   string `json:"time_fish_pulled"`
			TimeFishAnesth   string `json:"time_fish_anesth"`
			TimeDissections  string `json:"time_dissections"`
			//TissueSample     int    `json:"tissue_sample"`
		} `json:"entries"`
	} `json:"data"`
}
