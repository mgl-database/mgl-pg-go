package sep_epi

import (
	"encoding/json"
	"time"

	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

var EDNAReplicates map[string]string

func (a EDNA_branch_1) GetLinks() epi.Links {
	return a.Links
}

func (h EDNA_branch_1) GetData() interface{} {
	return h.Data
}

func (a EDNA_branch_1) GetEntries() []byte {
	exclude := []string{"title", "edna_MGL_Prefix"}
	entries := make([]interface{}, len(a.Data.Entries))
	for i, v := range a.Data.Entries {
		// Convert the struct to a map
		var entryMap map[string]interface{}

		//sort suffix
		if v.EdnaMGLPrefix != "" {
			*v.Filter1 = v.EdnaMGLPrefix + *v.Filter1
			if *v.Filter2 != "" {
				*v.Filter2 = v.EdnaMGLPrefix + *v.Filter2
			} else {
				v.Filter2 = nil
			}
			if *v.Filter3 != "" {
				*v.Filter3 = v.EdnaMGLPrefix + *v.Filter3
			} else {
				v.Filter3 = nil
			}
			if *v.Filter4 != "" {
				*v.Filter4 = v.EdnaMGLPrefix + *v.Filter4
			} else {
				v.Filter4 = nil
			}
		}

		//marshal the entry
		inrec, _ := json.Marshal(v)
		//unmarshal the entry into the entry map
		json.Unmarshal(inrec, &entryMap)

		// Remove unwanted fields
		for _, exc := range exclude {
			delete(entryMap, exc)
		}

		// Add the map to the entries slice
		entries[i] = entryMap
	}

	entryMarsh, err := json.Marshal(entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}

func (h EDNA_branch_1) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

type EDNA_branch_1 struct {
	epi.Links `json:"links"`
	epi.Meta  `json:"meta"`
	Data      struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5BranchOwnerUUID string    `json:"ec5_branch_owner_uuid"`
			Ec5BranchUUID      string    `json:"ec5_branch_uuid"`
			CreatedAt          time.Time `json:"created_at"`
			UploadedAt         time.Time `json:"uploaded_at"`
			CreatedBy          string    `json:"created_by"`
			//Title              string        `json:"title"`
			EdnaSampler     string                   `json:"edna_Sampler"`
			DateMdy         epi.PSQLDate             `json:"edna_date"`
			WaterSource     epi.StrArrToStr          `json:"WATER_SOURCE"`
			KitNum          epi.StringOrInt          `json:"Kit_num"`
			NegativeControl epi.StrToBool            `json:"Negative_Control"`
			TempC           epi.FloatOrStringToFloat `json:"Temp_C"`
			DOPerc          epi.FloatOrStringToFloat `json:"dO_perc"`
			DOMgL           epi.FloatOrStringToFloat `json:"DO_mgL"`
			SalinityPpt     epi.FloatOrStringToFloat `json:"Salinity_ppt"`
			Weather         epi.StrArrToStr          `json:"Weather"`
			TimeStartedFilt string                   `json:"Time_started_filt"`
			EdnaMGLPrefix   string                   `json:"edna_MGL_Prefix"`
			Filter1         *string                  `json:"Filter_1"`
			Filter2         *string                  `json:"Filter_2"`
			Filter3         *string                  `json:"Filter_3"`
			Filter4         *string                  `json:"Filter_4"`
			TimeEndedFilter string                   `json:"Time_ended_filter"`
			VolumeFilteredL epi.FloatOrStringToFloat `json:"Volume_Filtered_l"`
			DepthFilteredM  epi.FloatOrStringToFloat `json:"Depth_Filtered_m"`
			Tide            string                   `json:"tide"`
			FilterType      string                   `json:"filter_type"`
			Comments        string                   `json:"COMMENTS"`
		} `json:"entries"`
	} `json:"data"`
}

// needed to convert into the MGL eDNA format
func (ednaPage *EDNA_branch_1) SplitEDNAIDsByFilterAndApplyPrefix() {
	EDNAReplicates = make(map[string]string)
	endSlice := len(ednaPage.Data.Entries)
	//using len so that we don't loop over the appended slices
	for i, ednaEntry := range ednaPage.Data.Entries[:endSlice] {
		//create new entries for each time a replicate filter is found
		if *ednaEntry.Filter2 != "" {
			EDNAReplicates[*ednaEntry.Filter2] = *ednaPage.Data.Entries[i].Filter1
			newEntry := ednaEntry
			newEntry.Filter1 = ednaEntry.Filter2
			newEntry.Ec5BranchUUID = ednaEntry.Ec5BranchUUID + "_REPLICATE_1"
			ednaPage.Data.Entries = append(ednaPage.Data.Entries, newEntry)
		}
		if *ednaEntry.Filter3 != "" {
			EDNAReplicates[*ednaEntry.Filter3] = *ednaPage.Data.Entries[i].Filter1
			newEntry := ednaEntry
			newEntry.Filter1 = ednaEntry.Filter3
			newEntry.Ec5BranchUUID = ednaEntry.Ec5BranchUUID + "_REPLICATE_2"
			ednaPage.Data.Entries = append(ednaPage.Data.Entries, newEntry)
		}
		if *ednaEntry.Filter4 != "" {
			EDNAReplicates[*ednaEntry.Filter4] = *ednaPage.Data.Entries[i].Filter1
			newEntry := ednaEntry
			newEntry.Filter1 = ednaEntry.Filter4
			newEntry.Ec5BranchUUID = ednaEntry.Ec5BranchUUID + "_REPLICATE_3"
			ednaPage.Data.Entries = append(ednaPage.Data.Entries, newEntry)
		}
	}
}

//these need to be copied to the mgl table
//	Ec5BranchUUID      string    `json:"ec5_branch_uuid"`
//EdnaSampler     string        `json:"edna_Sampler"`
//DateMdy         PSQLDate      `json:"edna_date"`
//TempC           FloatOrString `json:"Temp_C"`
//DOMgL           FloatOrString `json:"DO_mgL"`
//SalinityPpt     FloatOrString `json:"Salinity_ppt"`
//Weather         StrArrToStr   `json:"Weather"`
//TimeStartedFilt string        `json:"Time_started_filt"`
//Filter1         string        `json:"Filter_1"`
//	TimeEndedFilter string        `json:"Time_ended_filter"`
//	VolumeFilteredL FloatOrString `json:"Volume_Filtered_l"`
//	DepthFilteredM  FloatOrString `json:"Depth_Filtered_m"`
//	FilterType      string        `json:"filter_type"`
//Comments        string        `json:"COMMENTS"`
