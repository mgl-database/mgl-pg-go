package epi_sep_dailyhatchery

import (
	"encoding/json"

	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"

	Logging "gitlab.com/UrsusArcTech/logger"
)

func (a Hatchery_form_2) GetLinks() epi.Links {
	return a.Links
}

func (h Hatchery_form_2) GetData() interface{} {
	return h.Data
}

func (a Hatchery_form_2) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}

func (h Hatchery_form_2) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

type Hatchery_form_2 struct {
	epi.Links `json:"links"`
	epi.Meta  `json:"meta"`
	Data      struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID       string            `json:"ec5_uuid"`
			Ec5ParentUUID string            `json:"ec5_parent_uuid"`
			CreatedAt     epi.TimeWTimeZone `json:"created_at"`
			UploadedAt    epi.TimeWTimeZone `json:"uploaded_at"`
			CreatedBy     string            `json:"created_by"`
			///Title         string            `json:"title"`
			HatcheryName string `json:"hatchery_name"`
		} `json:"entries"`
	} `json:"data"`
}
