package sep_epi

import (
	"encoding/json"

	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

func (h Cohort_form_3) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

type Cohort_form_3 struct {
	epi.Links `json:"links"`
	epi.Meta  `json:"meta"`
	Data      struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID       string            `json:"ec5_uuid"`
			Ec5ParentUUID string            `json:"ec5_parent_uuid"`
			CreatedAt     epi.TimeWTimeZone `json:"created_at"`
			UploadedAt    epi.TimeWTimeZone `json:"uploaded_at"`
			CreatedBy     string            `json:"created_by"`
			//Title           string    `json:"title"`
			//NegativeControl string      `json:"negative_Control"`
			Species epi.StrArrToStr `json:"species"`
			//CohortNameForFi   string    `json:"cohort_Name_for_fi"`
			PreviousCohorts []string `json:"previous_Cohorts"`
			CohortNameForDb string   `json:"cohort_name_for_db"`
			Type            string   `json:"type"`

			//NewCohortNameNeed string `json:"new_cohort_name_need"`
		} `json:"entries"`
	} `json:"data"`
}

func (a Cohort_form_3) GetLinks() epi.Links {
	return a.Links
}

func (a Cohort_form_3) GetData() interface{} {
	//there are some fields we don't want to submit to the database from here

	return a.Data
}

func (a Cohort_form_3) GetEntries() []byte {
	exclude := []string{"previous_Cohorts"}
	entries := make([]interface{}, len(a.Data.Entries))
	for i, v := range a.Data.Entries {
		// Convert the struct to a map
		var entryMap map[string]interface{}
		//marshal the entry
		inrec, _ := json.Marshal(v)
		//unmarshal the entry into the entry map
		json.Unmarshal(inrec, &entryMap)

		// Remove unwanted fields
		for _, exc := range exclude {
			delete(entryMap, exc)
		}

		// Add the map to the entries slice
		entries[i] = entryMap
	}

	entryMarsh, err := json.Marshal(entries)
	if err != nil {
		Logging.LogError(err.Error())
	}
	return entryMarsh
}
