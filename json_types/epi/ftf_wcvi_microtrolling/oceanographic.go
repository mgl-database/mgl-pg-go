package epi_ftf_wcvi_microtrolling

import (
	"encoding/json"
	"time"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

type Oceanographic struct {
	epi.Links   `json:"links"`
	epi.Meta    `json:"meta"`
	epi.Mapping `json:"mapping"`
	Data        struct {
		ID      epi.StringNoNa `json:"id"`
		Type    epi.StringNoNa `json:"type"`
		Entries []struct {
			Ec5BranchOwnerUUID epi.StringNoNa `json:"ec5_branch_owner_uuid"`
			Ec5BranchUUID      epi.StringNoNa `json:"ec5_branch_uuid"`
			CreatedAt          time.Time      `json:"created_at"`
			UploadedAt         time.Time      `json:"uploaded_at"`
			CreatedBy          epi.StringNoNa `json:"created_by"`
			Title              epi.StringNoNa `json:"title"`
			One2Coordinates    struct {
				Latitude    epi.FloatOrStringToFloat `json:"latitude"`
				Longitude   epi.FloatOrStringToFloat `json:"longitude"`
				Accuracy    epi.StringOrIntToInt     `json:"accuracy"`
				UTMNorthing epi.StringOrIntToInt     `json:"UTM_Northing"`
				UTMEasting  epi.StringOrIntToInt     `json:"UTM_Easting"`
				UTMZone     epi.StringNoNa           `json:"UTM_Zone"`
			} `json:"12_Coordinates"`
			One3Time              epi.StringNoNa           `json:"13_Time"`
			One5Tide              epi.StringNoNa           `json:"15_Tide"`
			One6Weather           epi.StringNoNa           `json:"16_Weather"`
			One7BeaufortWindSca   epi.StringNoNa           `json:"17_Beaufort_Wind_Sca"`
			One8WaterQualityMea   epi.StringNoNa           `json:"18_Water_quality_mea"`
			Two0TemperatureC      epi.FloatOrStringToFloat `json:"20_Temperature_C"`
			Two1DOMgL             epi.FloatOrStringToFloat `json:"21_DO_mgL"`
			Two2SalinityPSU       epi.FloatOrStringToFloat `json:"22_Salinity_PSU"`
			Two3ConductivityScm   epi.FloatOrStringToFloat `json:"23_Conductivity_scm"`
			Two4PH                epi.FloatOrStringToFloat `json:"24_pH"`
			Two5TurbidityIndex    epi.StringOrIntToInt     `json:"25_Turbidity_index"`
			Two7TemperatureC      epi.FloatOrStringToFloat `json:"27_Temperature_C"`
			Two8DOMgL             epi.FloatOrStringToFloat `json:"28_DO_mgL"`
			Two9SalinityPSU       epi.FloatOrStringToFloat `json:"29_Salinity_PSU"`
			Three0ConductivityScm epi.FloatOrStringToFloat `json:"30_Conductivity_scm"`
			Three1PH              epi.FloatOrStringToFloat `json:"31_pH"`
			Three2TurbidityIndex  epi.StringOrIntToInt     `json:"32_Turbidity_index"`
			Three4TemperatureC    epi.FloatOrStringToFloat `json:"34_Temperature_C"`
			Three5DOMgL           epi.FloatOrStringToFloat `json:"35_DO_mgL"`
			Three6SalinityPSU     epi.FloatOrStringToFloat `json:"36_Salinity_PSU"`
			Three7ConductivityScm epi.FloatOrStringToFloat `json:"37_Conductivity_scm"`
			Three8PH              epi.FloatOrStringToFloat `json:"38_pH"`
			Three9TurbidityIndex  epi.StringOrIntToInt     `json:"39_Turbidity_index"`
			Four1TemperatureC     epi.FloatOrStringToFloat `json:"41_Temperature_C"`
			Four2DOMgL            epi.FloatOrStringToFloat `json:"42_DO_mgL"`
			Four3SalinityPSU      epi.FloatOrStringToFloat `json:"43_Salinity_PSU"`
			Four4ConductivityScm  epi.FloatOrStringToFloat `json:"44_Conductivity_scm"`
			Four5PH               epi.FloatOrStringToFloat `json:"45_pH"`
			Four6TurbidityIndex   epi.StringOrIntToInt     `json:"46_Turbidity_index"`
			Four8TemperatureC     epi.FloatOrStringToFloat `json:"48_Temperature_C"`
			Four9DOMgL            epi.FloatOrStringToFloat `json:"49_DO_mgL"`
			Five0SalinityPSU      epi.FloatOrStringToFloat `json:"50_Salinity_PSU"`
			Five1ConductivityScm  epi.FloatOrStringToFloat `json:"51_Conductivity_scm"`
			Five2PH               epi.FloatOrStringToFloat `json:"52_pH"`
			Five3TurbidityIndex   epi.StringOrIntToInt     `json:"53_Turbidity_index"`
			Five5TemperatureC     epi.FloatOrStringToFloat `json:"55_Temperature_C"`
			Five6DOMgL            epi.FloatOrStringToFloat `json:"56_DO_mgL"`
			Five7SalinityPSU      epi.FloatOrStringToFloat `json:"57_Salinity_PSU"`
			Five8ConductivityScm  epi.FloatOrStringToFloat `json:"58_Conductivity_scm"`
			Five9PH               epi.FloatOrStringToFloat `json:"59_pH"`
			Six0TurbidityIndex    epi.StringOrIntToInt     `json:"60_Turbidity_index"`
			Six2TemperatureC      epi.FloatOrStringToFloat `json:"62_Temperature_C"`
			Six3DOMgL             epi.FloatOrStringToFloat `json:"63_DO_mgL"`
			Six4SalinityPSU       epi.FloatOrStringToFloat `json:"64_Salinity_PSU"`
			Six5ConductivityScm   epi.FloatOrStringToFloat `json:"65_Conductivity_scm"`
			Six6PH                epi.FloatOrStringToFloat `json:"66_pH"`
			Six7Turbidity         epi.StringOrIntToInt     `json:"67_Turbidity"`
			Six8Comments          epi.StringNoNa           `json:"68_Comments"`
		} `json:"entries"`
	} `json:"data"`
}

func (h Oceanographic) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a Oceanographic) GetLinks() epi.Links {
	return a.Links
}

func (a Oceanographic) GetData() interface{} {
	return a.Data
}

func (a Oceanographic) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		logger.LogError(err.Error())
	}
	return entryMarsh
}
