package epi_ftf_wcvi_microtrolling

import (
	"encoding/json"
	"time"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

type FishingSet struct {
	epi.Links   `json:"links"`
	epi.Meta    `json:"meta"`
	epi.Mapping `json:"mapping"`
	Data        struct {
		ID      epi.StringNoNa `json:"id"`
		Type    epi.StringNoNa `json:"type"`
		Entries []struct {
			Ec5BranchOwnerUUID     epi.StringNoNa           `json:"ec5_branch_owner_uuid"` //ConvParentUUID
			Ec5BranchUUID          epi.StringNoNa           `json:"ec5_branch_uuid"`       //Fishingset
			CreatedAt              time.Time                `json:"created_at"`
			UploadedAt             time.Time                `json:"uploaded_at"`
			CreatedBy              epi.StringNoNa           `json:"created_by"`
			Title                  epi.StringNoNa           `json:"title"`
			Seven1FishingSet       epi.StringOrIntToInt     `json:"71_Fishing_Set_"`     //Set
			Latitude               epi.FloatOrStringToFloat `json:"lat_72_Coordinates"`  //Latitude
			Longitude              epi.FloatOrStringToFloat `json:"long_72_Coordinates"` //Longitude
			Accuracy               epi.StringOrIntToInt     `json:"accuracy_72_Coordinates"`
			UTMNorthing            epi.StringOrIntToInt     `json:"UTM_Northing_72_Coordinates"`
			UTMEasting             epi.StringOrIntToInt     `json:"UTM_Easting_72_Coordinates"`
			UTMZone                epi.StringNoNa           `json:"UTM_Zone_72_Coordinates"` //Timezone
			Seven3BottomDepthFt    epi.FloatOrStringToFloat `json:"73_Bottom_Depth_ft"`      //BottomDepthFt
			Seven4SetStartTimeFi   epi.StringNoNa           `json:"74_Set_Start_Time_fi"`    //StartTime
			Seven6SidesFished      []epi.StringNoNa         `json:"76_Sides_Fished"`         //SetComments
			Seven7HookIntervalFt   epi.StringOrIntToInt     `json:"77_Hook_Interval_ft"`     //SetComments
			Seven8PortCannonballD  epi.StringOrIntToInt     `json:"78_Port_Cannonball_D"`    //SetComments
			Seven9StarboardCannonb epi.StringOrIntToInt     `json:"79_Starboard_Cannonb"`    //SetComments
			Eight0OfPortHooks      epi.StringOrIntToInt     `json:"80__of_Port_Hooks"`       //SetComments
			Eight1OfStarboardHoo   epi.StringOrIntToInt     `json:"81__of_Starboard_Hoo"`
			Eight2Comments         epi.StringNoNa           `json:"82_Comments"`          //SetComments
			Eight3SetEndTimeLast   epi.StringNoNa           `json:"83_Set_End_Time_last"` //EndTime
			Eight5WasAnythingCaug  epi.StringNoNa           `json:"85_Was_anything_caug"`
			Eight6Comments         epi.StringNoNa           `json:"86_Comments"` //SetComments
		} `json:"entries"`
	} `json:"data"`
}

func (h FishingSet) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a FishingSet) GetLinks() epi.Links {
	return a.Links
}

func (a FishingSet) GetData() interface{} {
	return a.Data
}

func (a FishingSet) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		logger.LogError(err.Error())
	}
	return entryMarsh
}
