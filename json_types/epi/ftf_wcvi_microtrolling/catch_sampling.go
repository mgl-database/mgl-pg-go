package epi_ftf_wcvi_microtrolling

import (
	"encoding/json"
	"time"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

type CatchSampling struct {
	epi.Links   `json:"links"`
	epi.Meta    `json:"meta"`
	epi.Mapping `json:"mapping"`
	Data        struct {
		ID      epi.StringNoNa `json:"id"`
		Type    epi.StringNoNa `json:"type"`
		Entries []struct {
			Ec5BranchOwnerUUID    epi.StringNoNa           `json:"ec5_branch_owner_uuid"` //ConvParentUUID
			Ec5BranchUUID         epi.StringNoNa           `json:"ec5_branch_uuid"`       //SpecimenUniqueID
			CreatedAt             time.Time                `json:"created_at"`
			UploadedAt            time.Time                `json:"uploaded_at"`
			CreatedBy             epi.StringNoNa           `json:"created_by"`
			Title                 epi.StringNoNa           `json:"title"`
			Eight9FishingSet      epi.StringOrIntToInt     `json:"89_Fishing_Set_"` //convFishingsetID
			Nine0Hook             epi.StringNoNa           `json:"90_Hook"`
			Nine1Species          epi.StringNoNa           `json:"91_Species"` //convSpeciesName
			Nine2SamplingThisChi  epi.StringNoNa           `json:"92_Sampling_this_Chi"`
			Nine3Recapture        epi.StringNoNa           `json:"93_Recapture"`
			Nine4RecapturedPITTa  epi.StringNoNa           `json:"94_Recaptured_PIT_ta"`
			Nine6ClinicalSignsCh  epi.StringNoNa           `json:"96_Clinical_Signs_ch"` //SpecimenComments2
			Nine7OfAdultMotile    epi.StringOrIntToInt     `json:"97__of_Adult_Motile_"`
			Nine8ClinicalComments epi.StringNoNa           `json:"98_Clinical_Comments"`
			One00AdiposeClipSta   epi.StringNoNa           `json:"100_Adipose_Clip_Sta"`
			One01ForkLengthMm     epi.FloatOrStringToFloat `json:"101_Fork_Length_mm"` //ForkLengthMm
			One02HeightMm         epi.FloatOrStringToFloat `json:"102_Height_mm"`
			One04FinVial          epi.StringNoNa           `json:"104_Fin_Vial_"`
			One05GillVial         epi.StringNoNa           `json:"105_Gill_Vial_"` //FishNum
			One06TimeGillSample   epi.StringNoNa           `json:"106_Time_Gill_Sample"`
			One07PITTagApplied    epi.StringNoNa           `json:"107_PIT_Tag_Applied"`
			One09PitTagTrayPh     epi.StringNoNa           `json:"109_Pit_Tag_Tray__Ph"`
			One10PITTagLast4      epi.StringNoNa           `json:"110_PIT_Tag__last_4_"`
			One11PitTagComments   epi.StringNoNa           `json:"111_Pit_Tag_Comments"`
			One12LethalSample     epi.StringNoNa           `json:"112_Lethal_Sample"` //NonlethalYn - inverted
			One14TypeOfLethalS    epi.StringNoNa           `json:"114_Type_of_Lethal_S"`
			One15TakingAPhoto     epi.StringNoNa           `json:"115_Taking_a_Photo"`
			One16Photo            epi.StringNoNa           `json:"116_Photo"`
			One17Comments         epi.StringNoNa           `json:"117_Comments"` //SpecimenComments
		} `json:"entries"`
	} `json:"data"`
}

func (h CatchSampling) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a CatchSampling) GetLinks() epi.Links {
	return a.Links
}

func (a CatchSampling) GetData() interface{} {
	return a.Data
}

func (a CatchSampling) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		logger.LogError(err.Error())
	}
	return entryMarsh
}
