package epi_ftf_wcvi_microtrolling

import (
	"encoding/json"
	"time"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_types/epi"
)

type BaseEntries struct {
	epi.Links   `json:"links"`
	epi.Meta    `json:"meta"`
	epi.Mapping `json:"mapping"`
	Data        struct {
		ID      string `json:"id"`
		Type    string `json:"type"`
		Entries []struct {
			Ec5UUID               string    `json:"ec5_uuid"` //AqID
			CreatedAt             time.Time `json:"created_at"`
			UploadedAt            time.Time `json:"uploaded_at"`
			CreatedBy             string    `json:"created_by"`
			Title                 string    `json:"title"`
			TwoDate               string    `json:"2_Date"`
			ThreeSurveyType       string    `json:"3_Survey_Type"`
			FourSoundArea         string    `json:"4_SoundArea"`
			FiveFishingLocation   string    `json:"5_Fishing_Location"` //SetLocation
			SixVesselName         string    `json:"6_Vessel_Name"`      //Vessel
			SevenOrganizationEgFi string    `json:"7_Organization_eg_Fi"`
			EightCrewMembersFull  string    `json:"8_Crew_Members_full_"`
			NineCommentsOtherVes  string    `json:"9_Comments_other_ves"`
			One0OCEANOGRAPHICDET  int       `json:"10_OCEANOGRAPHIC_DET"`
			Six9FISHINGSETS       int       `json:"69_FISHING_SETS"`
			Eight7CATCHSAMPLING   int       `json:"87_CATCH__SAMPLING"`
		} `json:"entries"`
	} `json:"data"`
}

func (h BaseEntries) GetGenericEntries() []interface{} {
	entries := make([]interface{}, len(h.Data.Entries))
	for i, v := range h.Data.Entries {
		entries[i] = v
	}
	return entries
}

func (a BaseEntries) GetLinks() epi.Links {
	return a.Links
}

func (a BaseEntries) GetData() interface{} {
	return a.Data
}

func (a BaseEntries) GetEntries() []byte {
	entryMarsh, err := json.Marshal(a.Data.Entries)
	if err != nil {
		logger.LogError(err.Error())
	}
	return entryMarsh
}
