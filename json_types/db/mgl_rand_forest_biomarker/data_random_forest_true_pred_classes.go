package random_forest

type True_pred_class []struct {
	ClassifierID   int `json:"classifier_id"`
	PredictedClass int `json:"predicted_class"`
	RfTpcID        int `json:"rf_tpc_id"`
	SpecimenID     int `json:"specimen_id"`
	TrueClass      int `json:"true_class"`
}
