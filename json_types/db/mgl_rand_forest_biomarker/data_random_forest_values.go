package random_forest

type Values []struct {
	Value Value
}

type Value struct {
	ClassID                  int     `json:"class_id"`
	ControlStudyFish         bool    `json:"control_study_fish"`
	ProjectID                int     `json:"project_id"`
	RfID                     int     `json:"rf_id"`
	SpecimenID               int     `json:"specimen_id"`
	EDNAID                   int     `json:"edna_id"`
	Value                    float64 `json:"value"`
	RandomForestSpecimenName *string `json:"random_forest_specimen_name"`
	TrayID                   int     `json:"tray_id"`
}
