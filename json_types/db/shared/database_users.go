package json_mgl_shared

type DatabaseUsers []struct {
	User_id    int
	First_name string
	Last_name  string
	Email      string
}
