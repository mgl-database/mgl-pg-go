package json_db_sep

import (
	"errors"
	"time"

	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
)

func init() {
	MetadataEDNAConversionMap = make(map[string][]string)
	MetadataEDNAConversionMap["SampleID"] = []string{"Filter1"}
	//	MetadataEDNAConversionMap["SampleID"] = []string{"EdnaMGLPrefix", "Filter1"}
	MetadataEDNAConversionMap["CollectionSamplers"] = []string{"EdnaSampler"}
	MetadataEDNAConversionMap["Date"] = []string{"DateMdy"}
	MetadataEDNAConversionMap["WaterTempC"] = []string{"TempC"}
	MetadataEDNAConversionMap["Weather"] = []string{"Weather"}
	MetadataEDNAConversionMap["DissolvedSolidsMgL"] = []string{"SalinityPpt"}
	MetadataEDNAConversionMap["DoMgL"] = []string{"DOMgL"}
	MetadataEDNAConversionMap["FilterStartTime"] = []string{"TimeStartedFilt"}
	MetadataEDNAConversionMap["FilterType"] = []string{"FilterType"}
	MetadataEDNAConversionMap["FilterComments"] = []string{"Comments"}
	MetadataEDNAConversionMap["FilterEndTime"] = []string{"TimeEndedFilter"}
	MetadataEDNAConversionMap["UniqueID"] = []string{"Ec5BranchUUID"}
	MetadataEDNAConversionMap["SampleVolumeMl"] = []string{"VolumeFilteredL"}
	MetadataEDNAConversionMap["SampleDepthM"] = []string{"DepthFilteredM"}
}

var MetadataEDNAConversionMap map[string][]string

type EDNA struct {
	ID int `json:"id"`
}

type Edna_epi_db struct {
	Aq              string    `json:"aq"`
	CohortNameForDb string    `json:"cohort_name_for_db"`
	Comments        string    `json:"comments"`
	Date            time.Time `json:"date"`
	DepthFilteredM  string    `json:"depth_filtered_m"`
	DoMgl           string    `json:"do_mgl"`
	DoPerc          string    `json:"do_perc"`
	Ec5BranchUUID   string    `json:"ec5_branch_uuid"`
	EdnaSampler     string    `json:"edna_sampler"`
	EdnaMGLPrefix   string    `json:"edna_MGL_Prefix"`
	Filter1         *string   `json:"filter_1"`
	Filter2         *string   `json:"filter_2"`
	Filter3         *string   `json:"filter_3"`
	Filter4         *string   `json:"filter_4"`
	FilterType      string    `json:"filter_type"`
	HatcheryName    string    `json:"hatchery_name"`
	NegativeControl bool      `json:"negative_control"`
	SalinityPpt     string    `json:"salinity_ppt"`
	Tank            string    `json:"tank"`
	TempC           string    `json:"temp_c"`
	Tide            string    `json:"tide"`
	TimeEndedFilter string    `json:"time_ended_filter"`
	TimeStartedFilt string    `json:"time_started_filt"`
	VolumeFilteredL string    `json:"volume_filtered_l"`
	Weather         string    `json:"weather"`
}

func GetEDNAID(name string) (int, error) {
	var edna []EDNA
	json_net_x.UnmarshalURL("http://127.0.0.1:8283/mgl/mgl-edna_metadata_uuid_pk?sample_id="+name, &edna, "GET")
	if len(edna) != 0 && edna[0].ID != 0 {
		if len(edna) > 1 {
			return -1, errors.New("Error: more than one eDNA record found with this ID:" + name)
		}
		return edna[0].ID, nil
	}
	return -1, errors.New("Unable to find eDNA name: " + name)
}
