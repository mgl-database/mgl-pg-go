package json_db_sep

import (
	Logging "gitlab.com/UrsusArcTech/logger"
	"gitlab.com/mgl-database/mgl-pg-go/json_net_x"
)

func init() {
	MetadataSpecimenConversionMap = make(map[string][]string)
	MetadataSpecimenConversionMap["FishNum"] = []string{"MGLPrefix", "TissueMGLNum"}
	MetadataSpecimenConversionMap["HistologyYn"] = []string{"HistoTaken"}
	MetadataSpecimenConversionMap["HistoJar"] = []string{"HistoJarNum"}
	MetadataSpecimenConversionMap["TotalLengthMm"] = []string{"LengthMm"}
	MetadataSpecimenConversionMap["MassG"] = []string{"WeightMg"}
	MetadataSpecimenConversionMap["ConditionLmd"] = []string{"Mort"}
	MetadataSpecimenConversionMap["SpecimenUniqueID"] = []string{"Ec5BranchUUID"}
	MetadataSpecimenConversionMap["SpecimenComments"] = []string{"Comments"}
	MetadataSpecimenConversionMap["DissectedBy"] = []string{"FishTimesSampler"}
	MetadataSpecimenConversionMap["DissectionDate"] = []string{"FishSampleDate"}
}

var MetadataSpecimenConversionMap map[string][]string

type MetadataSpecimen struct {
	SexMfu                 string  `json:"sex_mfu"`         //Sex             string      `json:"Sex"`
	HistologyYn            string  `json:"histology_yn"`    //HistoTaken      string      `json:"Histo_Taken"`
	HistoJar               string  `json:"histo_jar"`       //HistoJarNum     string      `json:"Histo_Jar_num"`
	TotalLengthMm          int     `json:"total_length_mm"` //LengthMm        int         `json:"Length_mm"`
	MassG                  float32 `json:"mass_g"`          //WeightMg        float64     `json:"weight_mg"`
	FishNum                string  `json:"fish_num"`        //TissueMGLNum    string      `json:"tissue_MGL_num"`
	DissectedBy            string  `json:"dissected_by"`    //FishTimesSampler string `json:"fish_times_Sampler"`
	DissectionDate         string  `json:"dissection_date"` //FishSampleDate   string `json:"fish_sample_date"`
	ConditionLmd           string  `json:"condition_lmd"`   //Mort            string      `json:"Mort"`
	SpeciesID              int     `json:"species_id"`
	HatcheryFishYn         string  `json:"hatchery_fish_yn"`
	HatcheryWild           string  `json:"hatchery_wild"`
	HatcheryWildDetermined string  `json:"hatchery_wild_determined"`
	SpecimenUniqueID       string  `json:"specimen_unique_id"` //Ec5BranchUUID      string        `json:"ec5_branch_uuid"`
	SpecimenComments       string  `json:"specimen_comments"`  //Comments string `json:"Comments"`
	SpecimenAq             string  `json:"specimen_aq"`
	SpecimenFishingsetID   *int    `json:"specimen_fishingset_id"`
}

type Species []struct {
	SpeciesId int `json:"species_id"`
}

func (m *MetadataSpecimen) GetDatabaseInfo() {
	var spp Species
	var sepTissuesView SepTissuesView

	//	Logging.LogMessage(m.FishNum)
	//not getting combined S + number IDs....
	json_net_x.UnmarshalURL("http://127.0.0.1:8282/mgl/mgl-view-all_sep_tissues?tissue_mgl_num="+string(m.FishNum), &sepTissuesView, "GET")

	if len(sepTissuesView) == 0 {
		Logging.LogError("API returned empty for: " + m.FishNum)
		return
	} else {
		m.SpecimenAq = sepTissuesView[0].Aq

	}

	json_net_x.UnmarshalURL("http://127.0.0.1:8282/mgl/mgl-species?common_name="+sepTissuesView[0].Species, &spp, "GET")

	if len(spp) == 0 {
		Logging.LogError("API returned empty for: " + sepTissuesView[0].Species)
		return
	} else {
		m.SpeciesID = spp[0].SpeciesId
	}

}
