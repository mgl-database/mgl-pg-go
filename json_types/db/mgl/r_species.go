package json_mgl

type RSpecies struct {
	CommonName         string `json:"common_name"`
	DateAdded          string `json:"date_added"`
	DateUpdated        string `json:"date_updated"`
	Family             string `json:"family"`
	Genus              string `json:"genus"`
	LegacySpeciesCode  any    `json:"legacy_species_code"`
	MarineGroup        string `json:"marine_group"`
	ScientificName     string `json:"scientific_name"`
	SpeciesDescription string `json:"species_description"`
	SpeciesID          int    `json:"species_id"`
}
