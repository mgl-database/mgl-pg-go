package json_mgl

// just a atomized count as it is used alot, instead of calling the whole specimen
type SpecimenCount []struct {
	Count int `json:"count"`
}

type MetadataSpecimen struct {
	AdiposeClipYn              string  `json:"adipose_clip_yn,omitempty"`
	AlternateNum               string  `json:"alternate_num,omitempty"`
	BloodHaematocritValue      string  `json:"blood_haematocrit_value,omitempty"`
	BloodHaematocritYn         string  `json:"blood_haematocrit_yn,omitempty"`
	BloodPlasmaVial            string  `json:"blood_plasma_vial,omitempty"`
	BloodPlasmaYn              string  `json:"blood_plasma_yn,omitempty"`
	BloodRbcYn                 string  `json:"blood_rbc_yn,omitempty"`
	BloodUsedYn                string  `json:"blood_used_yn,omitempty"`
	BloodYn                    string  `json:"blood_yn,omitempty"`
	CommentsBlood              string  `json:"comments_blood,omitempty"`
	CommentsCwt                string  `json:"comments_cwt,omitempty"`
	CommentsDissection         string  `json:"comments_dissection,omitempty"`
	CommentsDna                string  `json:"comments_dna,omitempty"`
	CommentsHisto              string  `json:"comments_histo,omitempty"`
	CommentsLengthMass         string  `json:"comments_length_mass,omitempty"`
	CommentsRna                string  `json:"comments_rna,omitempty"`
	CommentsSampleStorage      string  `json:"comments_sample_storage,omitempty"`
	ConditionLmd               string  `json:"condition_lmd,omitempty"`
	CwtID                      string  `json:"cwt_id,omitempty"`
	CwtYn                      string  `json:"cwt_yn,omitempty"`
	DissectedBy                string  `json:"dissected_by,omitempty"`
	DissectedYnp               string  `json:"dissected_ynp,omitempty"`
	DissectionDate             string  `json:"dissection_date,omitempty"`
	DissectionOrder            string  `json:"dissection_order,omitempty"`
	DnaID                      string  `json:"dna_id,omitempty"`
	DnaSeries                  string  `json:"dna_series,omitempty"`
	DnaVial                    string  `json:"dna_vial,omitempty"`
	DnaYn                      string  `json:"dna_yn,omitempty"`
	EstAge                     string  `json:"est_age,omitempty"`
	FishNum                    string  `json:"fish_num,omitempty"`
	ForkLengthMm               float64 `json:"fork_length_mm,omitempty"`
	FreshFrozenMeasuredWeighed string  `json:"fresh_frozen_measured_weighed,omitempty"`
	FrozenYn                   string  `json:"frozen_yn,omitempty"`
	GillAtpase1Yn              string  `json:"gill_atpase_1_yn,omitempty"`
	GillAtpase2Yn              string  `json:"gill_atpase_2_yn,omitempty"`
	HatcheryFishYn             string  `json:"hatchery_fish_yn,omitempty"`
	HatcheryWild               string  `json:"hatchery_wild,omitempty"`
	HistoBrainYn               string  `json:"histo_brain_yn,omitempty"`
	HistoEyeYn                 string  `json:"histo_eye_yn,omitempty"`
	HistoGillYn                string  `json:"histo_gill_yn,omitempty"`
	HistoHeadkidneyYn          string  `json:"histo_headkidney_yn,omitempty"`
	HistoHeartYn               string  `json:"histo_heart_yn,omitempty"`
	HistoIntestineYn           string  `json:"histo_intestine_yn,omitempty"`
	HistoJar                   string  `json:"histo_jar,omitempty"`
	HistoLiverYn               string  `json:"histo_liver_yn,omitempty"`
	HistoMuscleYn              string  `json:"histo_muscle_yn,omitempty"`
	HistoPostkidneyYn          string  `json:"histo_postkidney_yn,omitempty"`
	HistoProcessedYn           string  `json:"histo_processed_yn,omitempty"`
	HistoPyloriccaecaYn        string  `json:"histo_pyloriccaeca_yn,omitempty"`
	HistoSpleenYn              string  `json:"histo_spleen_yn,omitempty"`
	HistologyYn                string  `json:"histology_yn,omitempty"`
	HwDesignated               string  `json:"hw_designated,omitempty"`
	JuvAd                      string  `json:"juv_ad,omitempty"`
	Lifestage                  string  `json:"lifestage,omitempty"`
	MassG                      string  `json:"mass_g,omitempty"`
	Muscle1                    string  `json:"muscle_1,omitempty"`
	Muscle2                    string  `json:"muscle_2,omitempty"`
	MuscleYn                   string  `json:"muscle_yn,omitempty"`
	NonlethalYn                string  `json:"nonlethal_yn,omitempty"`
	OtolithCount               string  `json:"otolith_count,omitempty"`
	OtolithPosition            string  `json:"otolith_position,omitempty"`
	OtolithTray                string  `json:"otolith_tray,omitempty"`
	ProblemDescription         string  `json:"problem_description,omitempty"`
	ProblemFlag                string  `json:"problem_flag,omitempty"`
	RearingStrategy            string  `json:"rearing_strategy,omitempty"`
	RnaBrainYn                 string  `json:"rna_brain_yn,omitempty"`
	RnaGillYn                  string  `json:"rna_gill_yn,omitempty"`
	RnaHeadkidneyYn            string  `json:"rna_headkidney_yn,omitempty"`
	RnaHeartYn                 string  `json:"rna_heart_yn,omitempty"`
	RnaLiverYn                 string  `json:"rna_liver_yn,omitempty"`
	RnaMuscleYn                string  `json:"rna_muscle_yn,omitempty"`
	RnaSpleenBool              bool    `json:"rna_spleen_bool,omitempty"`
	RnaVial                    string  `json:"rna_vial,omitempty"`
	RnaYn                      string  `json:"rna_yn,omitempty"`
	ScalesAge                  string  `json:"scales_age,omitempty"`
	ScalesBook                 string  `json:"scales_book,omitempty"`
	ScalesL                    string  `json:"scales_l,omitempty"`
	ScalesPosition             string  `json:"scales_position,omitempty"`
	ScalesR                    string  `json:"scales_r,omitempty"`
	ScalesType                 string  `json:"scales_type,omitempty"`
	ScalesYn                   string  `json:"scales_yn,omitempty"`
	SealiceN                   string  `json:"sealice_n,omitempty"`
	SealiceYn                  string  `json:"sealice_yn,omitempty"`
	SexMfu                     string  `json:"sex_mfu,omitempty"`
	SpeciesID                  int     `json:"species_id,omitempty"`
	SpeciesUncertainYn         string  `json:"species_uncertain_yn,omitempty"`
	SpecimenAq                 string  `json:"specimen_aq,omitempty"`
	SpecimenComments           string  `json:"specimen_comments,omitempty"`
	SpecimenComments2          string  `json:"specimen_comments_2,omitempty"`
	SpecimenDateAdded          string  `json:"specimen_date_added,omitempty"`
	SpecimenDateUpdated        string  `json:"specimen_date_updated,omitempty"`
	SpecimenFishingsetID       int     `json:"specimen_fishingset_id,omitempty"`
	SpecimenUniqueID           string  `json:"specimen_unique_id,omitempty"`
	StockOrigin                string  `json:"stock_origin,omitempty"`
	StockOver60Calc            string  `json:"stock_over_60_calc,omitempty"`
	StomachGroup               string  `json:"stomach_group,omitempty"`
	StomachYn                  string  `json:"stomach_yn,omitempty"`
	TotalLengthMm              int     `json:"total_length_mm,omitempty"`
	WholeBloodSpunYn           string  `json:"whole_blood_spun_yn,omitempty"`
	PendingVialAssignment      bool    `json:"pending_vial_assignment,omitempty"`

	//for converstion - WILL NOT SUBMIT - THESe NEED TO BE REMOVED BEFORE BEING SENT
	//see epi.removeEntryFields
	ConvSpeciesName    string `json:"ConvSpeciesName,omitempty"`
	ConvFishingsetID   int    `json:"ConvFishingsetID,omitempty"`
	ConvParentUUID     string `json:"ConvParentUUID,omitempty"`
	ConvFishingsetUUID string `json:"ConvFishingsetUUID,omitempty"`

	SpecimenSpecimenID int `json:"specimen_specimen_id,omitempty"`
}
