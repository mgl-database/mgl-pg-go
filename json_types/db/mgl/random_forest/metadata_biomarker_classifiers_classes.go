package json_mgl_random_forest

type Classes []struct {
	ClassID      int    `json:"class_id"`
	ClassName    string `json:"class_name"`
	ClassifierID int    `json:"classifier_id"`
	Sensitivity  string `json:"sensitivity"`
	Specificity  string `json:"specificity"`
}
