package json_mgl_random_forest

type Classifiers []struct {
	ClassifierCreatorID int    `json:"classifier_creator_id"`
	ClassifierID        int    `json:"classifier_id"`
	ClassifierName      string `json:"classifier_name"`
}
