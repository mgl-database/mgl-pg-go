package json_mgl_random_forest

type Classifier_ranks []struct {
	BiomarkerID    int `json:"biomarker_id"`
	ClassifierID   int `json:"classifier_id"`
	ClassifierRank int `json:"classifier_rank"`
	RfID           int `json:"rf_id"`
}
