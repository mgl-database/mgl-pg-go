package json_mgl

type MetadataEdna struct {
	CollectionSamplers string  `json:"collection_samplers,omitempty"`   //EdnaSampler     string        `json:"edna_Sampler,omitempty"`
	Date               string  `json:"date,omitempty"`                  //DateMdy         PSQLDate      `json:"edna_date,omitempty"`
	WaterTempC         float32 `json:"water_temp_c,omitempty"`          //TempC           FloatOrString `json:"Temp_C,omitempty"`
	Weather            string  `json:"weather,omitempty"`               //Weather         StrArrToStr   `json:"Weather,omitempty"`
	DissolvedSolidsMgL float32 `json:"dissolved_solids_mg_l,omitempty"` //SalinityPpt     FloatOrString `json:"Salinity_ppt,omitempty"`
	DoMgL              float32 `json:"do_mg_l,omitempty"`               //DOMgL           FloatOrString `json:"DO_mgL,omitempty"`
	ReplicateOf        *int    `json:"replicate_of,omitempty"`          //       `json:"Filter_1,omitempty"` //Filter2         string        `json:"Filter_2,omitempty"`	//Filter3         string        `json:"Filter_3,omitempty"`	//Filter4         string        `json:"Filter_4,omitempty"`
	FilterStartTime    string  `json:"filter_start_time,omitempty"`     //TimeStartedFilt string        `json:"Time_started_filt,omitempty"`
	FilterType         string  `json:"filter_type,omitempty"`           //FilterType      string        `json:"filter_type,omitempty"`
	FilterComments     string  `json:"filter_comments,omitempty"`       //Comments        string        `json:"COMMENTS,omitempty"`
	FilterEndTime      string  `json:"filter_end_time,omitempty"`       //TimeEndedFilter string        `json:"Time_ended_filter,omitempty"`
	UniqueID           string  `json:"unique_id,omitempty"`             //Ec5BranchUUID      string    `json:"ec5_branch_uuid,omitempty"`
	//UniqueSet          *string    `json:"unique_set,omitempty"`
	SampleID       *string `json:"sample_id,omitempty"` //Filter1         string
	AqEdna         string  `json:"aq_edna,omitempty"`
	SampleVolumeMl float32 `json:"sample_volume_ml,omitempty"` //VolumeFilteredL FloatOrString `json:"Volume_Filtered_l,omitempty"`
	SampleDepthM   float32 `json:"sample_depth_m,omitempty"`   //DepthFilteredM  FloatOrString `json:"Depth_Filtered_m,omitempty"`
	ID             int     `json:"id,omitempty"`
}

/*
func (m *MetadataEdna) GetDatabaseInfo() {
	var sepEDNAView []MetadataEdna

	//Logging.LogMessage("Getting eDNA info for: " + *m.SampleID)
	//not getting combined S + number IDs....
	epi_get.UnmarshalURL("http://127.0.0.1:8282/mgl/get_sep_edna_by_filter?filter_num="+*m.SampleID, &sepEDNAView, "PATCH")

	if len(sepEDNAView) == 0 {
		Logging.LogError("eDNA API returned empty for: " + *m.SampleID)
		return
	} else {
		m.AqEdna = sepEDNAView[0].Aq
	}
}
*/
