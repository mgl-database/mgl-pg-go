package json_mgl

type MetadataTray struct {
	AlternateNum    *string `json:"alternate_num,omitempty"`
	BrainYn         *bool   `json:"brain_yn,omitempty"`
	Chamber         *int    `json:"chamber,omitempty"`
	Column_n        *int    `json:"column_n,omitempty"`
	CommentsOne     *string `json:"comments_one,omitempty"`
	CommentsTwo     *string `json:"comments_two,omitempty"`
	EdnaID          *int    `json:"edna_id,omitempty"`
	FishNum         *string `json:"fish_num,omitempty"`
	GillYn          *bool   `json:"gill_yn,omitempty"`
	HeartYn         *bool   `json:"heart_yn,omitempty"`
	IsControl       *bool   `json:"is_control,omitempty"`
	KidneyYn        *bool   `json:"kidney_yn,omitempty"`
	Lane            *int    `json:"lane,omitempty"`
	LiverYn         *bool   `json:"liver_yn,omitempty"`
	MixedSingle     *string `json:"mixed_single,omitempty"`
	MixedYn         *bool   `json:"mixed_yn,omitempty"`
	MuscleYn        *bool   `json:"muscle_yn,omitempty"`
	OtherYn         *bool   `json:"other_yn,omitempty"`
	Row             *string `json:"row,omitempty"`
	RunBy           *string `json:"run_by,omitempty"`
	SeaLiceYn       *bool   `json:"sea_lice_yn,omitempty"`
	Species         *string `json:"species,omitempty"`
	SpleenYn        *bool   `json:"spleen_yn,omitempty"`
	Tissue          *string `json:"tissue,omitempty"`
	TrayChamberID   *int    `json:"tray_chamber_id,omitempty"`
	TrayDateAdded   *string `json:"tray_date_added,omitempty"`
	TrayDateUpdated *string `json:"tray_date_updated,omitempty"`
	TrayName        *string `json:"tray_name,omitempty"`
	TrayNumber      *int    `json:"tray_number,omitempty"`
	TrayReusedYn    *int    `json:"tray_reused_yn,omitempty"`
	TraySpecimenID  *int    `json:"tray_specimen_id,omitempty"`
	UniqueIDMatched *string `json:"unique_id_matched,omitempty"`
	Well            *string `json:"well,omitempty"`
}
