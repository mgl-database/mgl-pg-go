package json_mgl

type Program []struct {
	Date_added    string `json:"date_added"`
	Date_updated  string `json:"date_updated"`
	Program_short string `json:"prog"`
	Program_id    int    `json:"prog_id"`
	Program_long  int    `json:"program"`
}
