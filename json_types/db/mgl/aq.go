package json_mgl

import "time"

type Aq []struct {
	Aq                       string     `json:"aq"`
	ChiefScientist           *string    `json:"chief_scientist"`
	ChiefScientistID         *int       `json:"chief_scientist_id"`
	Comments                 *string    `json:"comments"`
	CommentsCollectionMethod *string    `json:"comments_collection_method"`
	CommentsVialSeries       *string    `json:"comments_vial_series"`
	CruiseID                 *string    `json:"cruise_id"`
	EndDate                  *time.Time `json:"end_date"`
	MglLead                  *string    `json:"mgl_lead"`
	MglSamplers              *string    `json:"mgl_samplers"`
	MigratoryGroup           *string    `json:"migratory_group"`
	ProgID                   *int       `json:"prog_id"`
	SampleTypes              *string    `json:"sample_types"`
	SignoutName              *string    `json:"signout_name"`
	StartDate                *time.Time `json:"start_date"`
	Target                   *string    `json:"target"`
	Trip                     *string    `json:"trip"`
	TripLocation             *string    `json:"trip_location"`
	VialSeries               *string    `json:"vial_series"`
}
