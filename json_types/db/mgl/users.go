package json_mgl

type Users []struct {
	User_id    int
	First_name string
	Last_name  string
	Email      string
}
