package json_mgl

type MetadataFishingset struct {
	AqID               string `json:"aq_id"`
	BottomDepthFt      string `json:"bottom_depth_ft"` //Seven3BottomDepthFt
	CollectionSamplers string `json:"collection_samplers"`
	ConditionsComments string `json:"conditions_comments"`
	EndTime            string `json:"end_time"`
	FarmID             int    `json:"farm_id"`
	//FarmOrStationName  string `json:"farm_or_station_name"`
	Fishingset string `json:"fishingset"`

	GearType           string  `json:"gear_type"`
	Geom               string  `json:"geom"`
	HatcheryYn         string  `json:"hatchery_yn"`
	Latitude           float64 `json:"latitude"`
	LeadScientist      string  `json:"lead_scientist"`
	Longitude          float64 `json:"longitude"`
	PfmaArea           string  `json:"pfma_area"`
	PfmaAreaSubarea    string  `json:"pfma_area_subarea"`
	PfmaSubarea        string  `json:"pfma_subarea"`
	RArea              string  `json:"r_area"`
	SalinityPpt1M      string  `json:"salinity_ppt_1m"`
	SalinityPpt5M      string  `json:"salinity_ppt_5m"`
	SalinityPptSurface string  `json:"salinity_ppt_surface"`
	Set                string  `json:"set"`
	SetComments        string  `json:"set_comments"`
	SetDate            string  `json:"set_date"`
	SetLocation        string  `json:"set_location"`
	StartTime          string  `json:"start_time"` //
	Station            string  `json:"station"`
	SwFw               string  `json:"sw_fw"`
	TempC1M            string  `json:"temp_c_1m"`
	TempC5M            string  `json:"temp_c_5m"`
	TempSurfaceC       string  `json:"temp_c_surface"`
	TideComments       string  `json:"tide_comments"`
	Timezone           string  `json:"timezone"`
	TowDepthFt         string  `json:"tow_depth_ft"`
	Vessel             string  `json:"vessel"`

	//for converstion - WILL NOT SUBMIT - THESe NEED TO BE REMOVED BEFORE BEING SENT
	//see epi.removeEntryFields
	ConvParentUUID string

	FishingsetDateAdded   string `json:"fishingset_date_added"`
	FishingsetDateUpdated string `json:"fishingset_date_updated"`
	FishingsetID          int    `json:"fishingset_id"`
}
