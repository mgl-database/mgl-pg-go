package flags

import (
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"

	"gitlab.com/UrsusArcTech/logger"
	"gitlab.com/UrsusArcTech/logger/thread_safe_error_handling"
)

type flag struct {
	Description string
	Value       any
}

var flags map[string]flag

func init() {
	flags = make(map[string]flag)
	flags["-h"] = flag{
		Description: "Show this help menu.",
		Value:       "",
	}

}

func printFlags() {
	fmt.Printf("\n\n------------Help------------\n\n")
	for key, val := range flags {
		fmt.Printf("%s: %s\n", key, val.Description)
	}
	fmt.Printf("\n----------------------------\n\n")
}

func getHelpFlag() bool {
	for i := 0; i < len(os.Args[0:]); i++ {
		if os.Args[i] == "-h" {
			return true
		}
	}
	return false
}

func CheckHelpFlag() {
	if getHelpFlag() {
		printFlags()
	}
}

func GetFlagVal[T any](flagKey string) (T, error) {
	flagVal, exists := flags[flagKey]
	if !exists {
		return *new(T), fmt.Errorf("flag %s not found", flagKey)
	}

	val := flagVal.Value.(string)
	var result T
	switch any(result).(type) {
	case int:
		converted, err := strconv.Atoi(val)
		if err != nil {
			return *new(T), err
		}
		return any(converted).(T), nil
	case float64:
		converted, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return *new(T), err
		}
		return any(converted).(T), nil
	case string:
		return any(val).(T), nil
	case bool:
		converted, err := strconv.ParseBool(val)
		if err != nil {
			return *new(T), err
		}
		return any(converted).(T), nil
	default:
		return *new(T), fmt.Errorf("unsupported type")
	}
}

func CreateFlag[T any](flagKey string, defaultVal T, flagDescription string) T {
	var f flag
	setFlag := ""
	//see if flag has been set
	for i := 1; i < len(os.Args[1:]); i++ {
		if flagKey == os.Args[i] {
			if len(os.Args) < i+1 {
				break
			}
			setFlag = os.Args[i+1]
		}
	}

	//if not set to the default val
	if setFlag == "" {
		f = flag{
			Value:       defaultVal,
			Description: flagDescription,
		}
	} else {
		//if it has then try to convert it to the correct type
		convertedFlagVal, err := convertFlagValToType(setFlag, reflect.TypeOf(defaultVal).Kind())
		logger.CheckForErrorWithLevel(err, nil, thread_safe_error_handling.PANIC_ERR)

		//if it's the correct type then add it
		f = flag{
			Value:       convertedFlagVal,
			Description: flagDescription,
		}
	}

	flags[flagKey] = f
	return (f.Value).(T)
}

func convertFlagValToType(val string, typeWanted reflect.Kind) (any, error) {
	switch typeWanted {
	case reflect.Int:
		converted, err := strconv.Atoi(val)
		if err != nil {
			return nil, err
		}
		return converted, nil
	case reflect.Float64:
		converted, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return nil, err
		}
		return converted, nil
	case reflect.String:
		return val, nil
	case reflect.Bool:
		converted, err := strconv.ParseBool(val)
		if err != nil {
			return nil, err
		}
		return converted, nil
	default:
		return nil, errors.New("unsupported type")
	}
}

func DeleteFlag(flagKey string) {
	for i := 1; i < len(os.Args[1:]); i++ {
		if flagKey == os.Args[i] {
			os.Args[i] = ""
			if len(os.Args) < i+1 {
				break
			}
			os.Args[i+1] = ""
			return
		}
	}
	logger.LogError("Cannot find flag for deletion: " + flagKey)
}
